package com.justin.cablenetwork.utils;


import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LogWriter {

    private static final String FOLDER_NAME = "Logger";
    private static final String FILE_NAME = "log.txt";


    private static LogWriter mInstance = null;
    private FileWriter mFileWriter = null;
    private Context mContext = null;
    private String filePath = null;

    public LogWriter(Context context) {
        this.mContext = context;
    }

    public LogWriter(){

    }

    private String createLogFolder() {
        boolean created = true;
        File logFolder = new File(Environment.getExternalStorageDirectory(), FOLDER_NAME);
        if (!logFolder.exists())
            created = logFolder.mkdir();
        return created ? logFolder.getAbsolutePath().toString() : null;

    }

    private String getLogFilePath() {
        String folderPath = createLogFolder();
        if (folderPath == null)
            return null;
        return folderPath + "/" + FILE_NAME;
    }

    public static LogWriter getInstance(Context context) {
        if (mInstance == null)
            mInstance = new LogWriter(context);
        return mInstance;
    }

    public static LogWriter getInstance() {
        if (mInstance == null)
            mInstance = new LogWriter();
        return mInstance;
    }


//LogWriter.getInstance().Log();
    public void Log(String fileContents) {
        String filePath = getLogFilePath();

        if (filePath != null) {
            try {
                FileWriter fileWriter = getFileWriter();
                if (fileWriter != null) {
                    fileWriter.write(fileContents);
                    fileWriter.write("\n");
                    fileWriter.close();
                } else {
                    Log.d("PushNotificationLog", "writeStringAsFile: FileWriter is null");
                }

            } catch (IOException e) {
                Log.d("PushNotificationLog", "writeStringAsFile: " + e);
            }
        } else {
            Log.d("PushNotificationLog", "writeStringAsFile: null file name");
        }

    }

    private FileWriter getFileWriter() throws IOException {
        return new FileWriter(getLogFilePath(), true);
    }
}
