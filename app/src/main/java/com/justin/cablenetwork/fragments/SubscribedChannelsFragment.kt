package com.justin.cablenetwork.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.nfc.tech.MifareUltralight
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.justin.cablenetwork.R
import com.justin.cablenetwork.adapters.SubscribedChannelsAdapter
import com.justin.cablenetwork.callbacks.FragmentSubscribedClickListener
import com.justin.cablenetwork.callbacks.SubscribedCallback
import com.justin.cablenetwork.dialogs.ChannelListDialog
import com.justin.cablenetwork.dialogs.SubscriptionDialogListener
import com.justin.cablenetwork.dialogs.SubscriptionDialogRecyclerListener
import com.justin.cablenetwork.models.*
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.utils.ItemOffsetDecoration
import com.justin.cablenetwork.utils.NetworkConnectionUtils
import kotlinx.android.synthetic.main.activity_home_acivity.*
import kotlinx.android.synthetic.main.fragment_channel_list.*

import kotlinx.android.synthetic.main.fragment_subscribed_channels.*
import kotlinx.android.synthetic.main.fragment_subscribed_channels.fra_progress
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


@SuppressLint("ValidFragment")
class SubscribedChannelsFragment(
    itemUserListModel: ItemUserListModel,
    mToken: String
) : AllChannelsBaseFragment(), FragmentSubscribedClickListener, View.OnClickListener {


    private val mItemUserListModel = itemUserListModel
    private val mToken = mToken
    private lateinit var mSubscribedCallback : SubscribedCallback
    private lateinit var mSubscribedChannelsAdapter : SubscribedChannelsAdapter
    private var mContext : Context? = null
    private val mSubscribedChannels = ArrayList<SubscribedChannel>()
    private var isLoading = false
    private val ROWS_PER_PAGE = 20
    private var PAGE_NUMBER = 0

    private val mSubscriptionList = LinkedList<Channel>()
    private val mUnSubscriptionList = LinkedList<Channel>()

    private val mTempSubscriptionList = LinkedList<Channel>()


    override fun onClick(v: View?) {
        mTempSubscriptionList.clear()
        for (item in mSubscribedChannels){

            if (item.isSelected){
                mTempSubscriptionList.add(Channel(item.packageId,item.name,item.price))
            }
            Log.d("TAG_LIST",""+item.toString())
        }

        val channelList = ChannelListDialog(
            activity as Context,
            "Subscribe channel",
            mTempSubscriptionList,
            this
        )
        channelList.show()
    }

    override fun onConfirm() {
        Toast.makeText(activity,"onConfirm SubscribedChannelsFragment", Toast.LENGTH_SHORT).show()

        if (!NetworkConnectionUtils.getConnectionStatus(activity as Context)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        context?.let { showSimpleProgressAlert(it) }

        val channelIds : String = getChannelIdsToString(mTempSubscriptionList)
        Log.d("TAG_IDS","$channelIds")

        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getUpdatesubscribedChannelList(
            mToken,
            mItemUserListModel.uniqueId!!,
            channelIds
        )

        Log.d("TAG_CALL","Token : $mToken , uniqueid : ${mItemUserListModel.uniqueId} , channelids :  $channelIds")

        call?.enqueue(object : Callback<UpdatesubscribedChannelListResponse> {
            override fun onResponse(
                call: Call<UpdatesubscribedChannelListResponse>,
                response: Response<UpdatesubscribedChannelListResponse>
            ) {
                hideSimpleProgressAlert()

                val result = response.body()
                if (result != null) {
                    if (result.success!!) {
                        getSubscribedChannels()
                        Toast.makeText(activity, "Successfully subscribed", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(activity, "Failed to subscribe", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<UpdatesubscribedChannelListResponse>, t: Throwable) {
                hideSimpleProgressAlert()
                Toast.makeText(activity, "Failed to subscribed", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun getChannelIdsToString(channels: LinkedList<Channel>): String {
        var ids : String = ""
        for (item in channels) {
            ids = "$ids,${item.id}"
        }
        if (ids.length == 0){
            return ""
        }
        return ids.substring(1)
    }

    override fun onSubscribed(channel: Channel) {

        val iterator = mUnSubscriptionList.iterator()
        while (iterator.hasNext()){
            if (iterator.next().id == channel.id) {
                iterator.remove()
                setFabButtonVisibility()
                return
            }
        }
        mSubscriptionList.add(channel)
        setFabButtonVisibility()
    }

    @SuppressLint("RestrictedApi")
    private fun setFabButtonVisibility() {
        if (mUnSubscriptionList.size == 1 || mSubscriptionList.size == 1 ){
            fab_act_deact.visibility = View.VISIBLE
        }else if(mUnSubscriptionList.size == 0 && mSubscriptionList.size == 0){
            fab_act_deact.visibility = View.GONE
        }
    }

    override fun onUnSubscribed(channel: Channel) {

        val iterator = mSubscriptionList.iterator()
        while (iterator.hasNext()){
            if (iterator.next().id == channel.id) {
                iterator.remove()
                setFabButtonVisibility()
                return
            }
        }
        mUnSubscriptionList.add(channel)
        setFabButtonVisibility()
    }


    override fun onUnSubscribed(id: Long?) {
        Log.d("TAG_JUSTIN","")
    }



    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
        mSubscribedCallback = context as SubscribedCallback
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_subscribed_channels, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {

        fab_act_deact.setOnClickListener(this)

        mSubscribedChannelsAdapter = SubscribedChannelsAdapter(activity,mSubscribedChannels,this)
        val layoutManager = LinearLayoutManager(activity)
        recy_subscribed.layoutManager = layoutManager
        recy_subscribed.addItemDecoration(ItemOffsetDecoration(activity as Context, R.dimen.item_offset))
        recy_subscribed.adapter = mSubscribedChannelsAdapter

        recy_subscribed.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.getChildCount()
                val totalItemCount = layoutManager.getItemCount()
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if (!isLoading) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {
                        callSubscribedListAPI()
                        Snackbar.make(recy_subscribed, "Loading.....", Snackbar.LENGTH_SHORT).show()

                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        getSubscribedChannels()
    }

    private fun getSubscribedChannels() {
        PAGE_NUMBER = 0
        mSubscribedChannels.clear()
        mSubscriptionList.clear()
        mUnSubscriptionList.clear()
        mTempSubscriptionList.clear()
        setFabButtonVisibility()
        callSubscribedListAPI()
        setNoDataVisibility(false)
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser && isResumed){
            getSubscribedChannels()
        }
    }

    private fun callSubscribedListAPI() {
        if (!NetworkConnectionUtils.getConnectionStatus(activity as Context)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        if(PAGE_NUMBER == 0) {
            setProgressVisibility(true)
        }

        isLoading = true
        Log.d("TAG_CALL"," Subscribe fragment Token : $mToken , id ${mItemUserListModel.uniqueId!!}")
        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getSubscribedChannelList(
            mToken,
            mItemUserListModel.uniqueId!!,
            //1003,
            ROWS_PER_PAGE,
            PAGE_NUMBER
        )


        call?.enqueue(object : Callback<SubscribedChannelsResponse>{
            override fun onResponse(
                call: Call<SubscribedChannelsResponse>,
                response: Response<SubscribedChannelsResponse>
            ) {
                setProgressVisibility(false)
                isLoading = false

                val subscribedChannelsResponse = response.body()

                if (subscribedChannelsResponse?.success!!){
                    mSubscribedChannels.addAll(subscribedChannelsResponse?.data?.get(0)!!)
                    if (PAGE_NUMBER == 0) {
                        setNoDataVisibility(false)
                    }
                    PAGE_NUMBER++
                    mSubscribedChannelsAdapter.notifyDataSetChanged()
                }else{
                    if(PAGE_NUMBER == 0) {
                        Snackbar.make(
                            recy_subscribed,
                            subscribedChannelsResponse?.message!!,
                            Snackbar.LENGTH_LONG
                        )
                            .show()
                        setNoDataVisibility(true)
                    }
                }

            }

            override fun onFailure(call: Call<SubscribedChannelsResponse>, t: Throwable) {
                isLoading = false
                if (PAGE_NUMBER==0) {
                    Snackbar.make(
                        recy_subscribed,
                        "No data found!",
                        Snackbar.LENGTH_SHORT
                    )
                        .show()
                    setNoDataVisibility(true)
                }

                setProgressVisibility(false)
            }

        })
    }


    private fun setProgressVisibility(visibility : Boolean){
        if (visibility){
            fra_progress.visibility = View.VISIBLE
        }else{
            fra_progress.visibility = View.GONE
        }
    }


    private fun setNoDataVisibility(visibility : Boolean){
        if (visibility){
            fra_no_data.visibility = View.VISIBLE
        }else{
            fra_no_data.visibility = View.GONE
        }
    }

}
