package com.justin.cablenetwork.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.nfc.tech.MifareUltralight
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.justin.cablenetwork.dialogs.ChannelListDialog

import com.justin.cablenetwork.R
import com.justin.cablenetwork.adapters.AlaCarteChannelsAdapter
import com.justin.cablenetwork.callbacks.FragmentSubscribedClickListener
import com.justin.cablenetwork.callbacks.SubscribedCallback
import com.justin.cablenetwork.models.*
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.utils.ItemOffsetDecoration
import com.justin.cablenetwork.utils.NetworkConnectionUtils
import kotlinx.android.synthetic.main.activity_home_acivity.*
import kotlinx.android.synthetic.main.fragment_channel_list.*
import kotlinx.android.synthetic.main.fragment_channel_list.fra_progress
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


@SuppressLint("ValidFragment")
class AlaCarteChannelListFragment(itemUserListModel: ItemUserListModel, mToken: String) : AllChannelsBaseFragment(),
    FragmentSubscribedClickListener,
    View.OnClickListener {


    private val mItemUserListModel = itemUserListModel
    private val mToken = mToken
    private lateinit var mSubscribedCallback: SubscribedCallback
    private lateinit var mAllChannelsAdapter: AlaCarteChannelsAdapter
    private var mContext: Context? = null
    private val mAllChannels = ArrayList<ChannelInfo>()
    private var isLoading = false
    private val ROWS_PER_PAGE = 20
    private var PAGE_NUMBER = 0

    private val mSubscribedChannels = LinkedList<Channel>()


    @SuppressLint("RestrictedApi")
    override fun onSubscribed(channel: Channel) {
        //mSubscribedCallback.onSubscribed(channel)
        if (mSubscribedChannels.size == 0) {
            fab_subscribe.visibility = View.VISIBLE
        }

        mSubscribedChannels.add(channel)
    }


    override fun onUnSubscribed(id: Long?) {
        //mSubscribedCallback.onUnSubscribed(id)
        if (id != null) {
            val iterator = mSubscribedChannels.iterator()
            while (iterator.hasNext()) {
                if (iterator.next().id == id) {
                    iterator.remove()
                    break
                }
            }
            hideFabButton()
        }
    }

    @SuppressLint("RestrictedApi")
    fun hideFabButton(){
        if (mSubscribedChannels.size == 0) {
            fab_subscribe.visibility = View.GONE
        }
    }

    override fun onUnSubscribed(channel: Channel) {
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab_subscribe -> onShowChannelListClick()
        }
    }

    private fun onShowChannelListClick() {
        val channelList = ChannelListDialog(
            activity as Context,
            "Subscribe channels",
            mSubscribedChannels,
            this
        )
        channelList.show()
    }

    override fun onConfirm() {
        Toast.makeText(activity, "onConfirm : AllChannelsFragment ", Toast.LENGTH_SHORT).show()
        if (!NetworkConnectionUtils.getConnectionStatus(activity as Context)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        context?.let { showSimpleProgressAlert(it) }

        val channelIds : String = getChannelIdsToString(mSubscribedChannels)
        Log.d("TAG_IDS","$channelIds")

        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getSaveAllChannelsSelection(
            mToken,
            mItemUserListModel.uniqueId!!,
            channelIds
        )

        Log.d("TAG_CALL","Token : $mToken , uniqueid : ${mItemUserListModel.uniqueId} , channelids :  $channelIds")

        call?.enqueue(object : Callback<UpdatesubscribedChannelListResponse> {
            override fun onResponse(
                call: Call<UpdatesubscribedChannelListResponse>,
                response: Response<UpdatesubscribedChannelListResponse>
            ) {
                hideSimpleProgressAlert()

                val result = response.body()
                if (result != null) {
                    if (result.success!!) {
                        getAllChannels()
                        Toast.makeText(activity, "Successfully subscribed", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(activity, "Failed to subscribe", Toast.LENGTH_SHORT).show()
                    }
                }


            }

            override fun onFailure(call: Call<UpdatesubscribedChannelListResponse>, t: Throwable) {
                hideSimpleProgressAlert()
                Toast.makeText(activity, "Failed to subscribed", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun getChannelIdsToString(channels: LinkedList<Channel>): String {
        var ids : String = ""
        for (item in channels) {
            ids = "$ids,${item.id}"
        }
        return ids.substring(1)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
        mSubscribedCallback = context as SubscribedCallback
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ala_carte_channel_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onResume() {
        super.onResume()
        getAllChannels()
        Log.d("TAG_ALLCHANNELS","onResume all channels")

    }

    private fun getAllChannels() {
        PAGE_NUMBER = 0
        mAllChannels.clear()
        mSubscribedChannels.clear()
        hideFabButton()
        callChannelListAPI()
        setNoDataVisibility(false)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser && isResumed && !isLoading){
            Log.d("TAG_ALLCHANNELS","setUserVisibleHint all channels")
            getAllChannels()
        }
    }



    private fun callChannelListAPI() {

        if (!NetworkConnectionUtils.getConnectionStatus(activity as Context)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        if (PAGE_NUMBER == 0) {
            setProgressVisibility(true)
        }

        isLoading = true

        Log.d("TAG_CALL","All fragment Token : $mToken , id ${mItemUserListModel.uniqueId!!}")

        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getAlacarteChannels(
            mToken,
            mItemUserListModel.uniqueId!!,
            //1003,
            ROWS_PER_PAGE,
            PAGE_NUMBER
        )

        call?.enqueue(object : Callback<AllChannelsResponse> {
            override fun onResponse(
                call: Call<AllChannelsResponse>,
                response: Response<AllChannelsResponse>
            ) {
                setProgressVisibility(false)
                isLoading = false
                val subscribedChannelsResponse = response.body()

                if (subscribedChannelsResponse?.success!!) {
                    mAllChannels.addAll(subscribedChannelsResponse?.data?.get(0)!!)
                    if (PAGE_NUMBER == 0) {
                        setNoDataVisibility(false)
                    }
                    PAGE_NUMBER++
                    mAllChannelsAdapter.notifyDataSetChanged()
                } else {
                    if (PAGE_NUMBER == 0) {
                        Snackbar.make(
                            recy_all_channels,
                            subscribedChannelsResponse?.message!!,
                            Snackbar.LENGTH_LONG
                        )
                            .show()
                        setNoDataVisibility(true)
                    }
                }

            }

            override fun onFailure(call: Call<AllChannelsResponse>, t: Throwable) {
                isLoading = false
                if (PAGE_NUMBER == 0) {
                    Snackbar.make(
                        recy_all_channels,
                        "No data found!",
                        Snackbar.LENGTH_SHORT
                    )
                        .show()
                    setNoDataVisibility(true)
                }

                setProgressVisibility(false)
            }

        })

    }


    private fun initView() {

        fab_subscribe.setOnClickListener(this)

        mAllChannelsAdapter = AlaCarteChannelsAdapter(mContext, mAllChannels, this)
        val layoutManager = LinearLayoutManager(activity)
        recy_all_channels.layoutManager = layoutManager
        recy_all_channels.addItemDecoration(ItemOffsetDecoration(activity as Context, R.dimen.item_offset))
        recy_all_channels.adapter = mAllChannelsAdapter
        recy_all_channels.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.getChildCount()
                val totalItemCount = layoutManager.getItemCount()
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if (!isLoading) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= MifareUltralight.PAGE_SIZE
                    ) {
                        callChannelListAPI()
                        Snackbar.make(recy_all_channels, "Loading.....", Snackbar.LENGTH_SHORT).show()

                    }
                }
            }
        })
    }


    private fun setProgressVisibility(visibility: Boolean) {
        if (visibility) {
            fra_progress.visibility = View.VISIBLE
        } else {
            fra_progress.visibility = View.GONE
        }
    }

    private fun setNoDataVisibility(visibility: Boolean) {
        if (visibility) {
            fl_no_data.visibility = View.VISIBLE
        } else {
            fl_no_data.visibility = View.GONE
        }
    }

}
