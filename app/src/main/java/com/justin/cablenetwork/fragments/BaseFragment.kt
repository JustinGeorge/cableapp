package com.justin.cablenetwork.fragments

import android.app.ProgressDialog
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import com.justin.cablenetwork.R

open class BaseFragment : Fragment() {
    lateinit var builder: AlertDialog.Builder
    var mSimpleProgressDialog: ProgressDialog? = null

    lateinit var progressDialog: ProgressDialog

    fun showSimpleProgressAlert(context: Context) {
        // builder = AlertDialog.Builder(context)
        if (mSimpleProgressDialog != null && mSimpleProgressDialog!!.isShowing) {
            mSimpleProgressDialog?.hide()
        } else {
            mSimpleProgressDialog = ProgressDialog(context,  R.style.CustomProgressBarTheme)
        }
        mSimpleProgressDialog?.setMessage("Please wait!")
        mSimpleProgressDialog?.show()

    }

    fun hideSimpleProgressAlert() {
        if (mSimpleProgressDialog != null && mSimpleProgressDialog!!.isShowing) {
            mSimpleProgressDialog?.hide()
        }
    }


}