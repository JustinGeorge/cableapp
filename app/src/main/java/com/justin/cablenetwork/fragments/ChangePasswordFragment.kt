package com.justin.cablenetwork.fragments


import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.justin.cablenetwork.models.ChangePasswordModel
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import com.justin.cablenetwork.R
import com.justin.cablenetwork.utils.LogWriter
import com.justin.cablenetwork.utils.NetworkConnectionUtils
import kotlinx.android.synthetic.main.fragment_change_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ChangePasswordFragment : BaseFragment(), View.OnClickListener {

    lateinit var mContext: Context

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txt_change_password -> {
                /* startActivity(Intent(activity, HomeActivity::class.java))
                 activity?.overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right)*/
                if (NetworkConnectionUtils.getConnectionStatus(mContext)) {
                    verifiyPasswords()
                } else {
                    Snackbar.make(txt_change_password,resources.getString(R.string.no_connection), Snackbar.LENGTH_SHORT).show()
                }

            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.mContext = context!!
    }


    private fun verifiyPasswords() {
        val currentPassword = edtxt_current_password.text.trim().toString()
        val newPassword = edtxt_new_password.text.trim().toString()
        val rePassword = edtxt_re_password.text.trim().toString()
        Log.d("TAG_JUSTIN","Current password : "+currentPassword+" new password : "+newPassword+" repassword : "+rePassword)
        if (currentPassword == null || currentPassword.length < 4) {
            edtxt_current_password.setError(getString(R.string.txt_enter_valid_password))
            return
        } else if (newPassword == null || newPassword.length < 4) {
            edtxt_new_password.setError(getString(R.string.txt_enter_valid_password))
            return
        } else if (rePassword == null || rePassword.length < 4) {
            edtxt_re_password.setError(getString(R.string.txt_enter_valid_password))
            return
        } else if (!newPassword.equals(rePassword)) {
            Toast.makeText(activity, "Password mismatch!", Toast.LENGTH_SHORT).show()
            return
        }

        updatePassword(
            SharedPreferenceManager.getInstance(mContext).getString(PreferenceUtils.LoginConstants.mobile),
            currentPassword,
            newPassword
        )
    }

    private fun updatePassword(mobileNumber: String, currentPassword: String, newPassword: String) {
        showSimpleProgressAlert(mContext)
        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getChangePasswordResponse(mobileNumber, currentPassword, newPassword)
        call?.enqueue(object : Callback<ChangePasswordModel> {
            override fun onFailure(call: Call<ChangePasswordModel>, t: Throwable) {
                hideSimpleProgressAlert()
                Toast.makeText(mContext, "Unable to change password", Toast.LENGTH_SHORT).show()
                LogWriter.getInstance().Log("ChangePassword Failed response : "+t.message +"\n"+t.localizedMessage)

            }

            override fun onResponse(call: Call<ChangePasswordModel>, response: Response<ChangePasswordModel>) {
                hideSimpleProgressAlert()
                if (response.body()?.success!!) {
                    Toast.makeText(mContext, "Password updated successfully", Toast.LENGTH_SHORT).show()
                }
                LogWriter.getInstance().Log("ChangePassword Success response : "+response.body()!!.toString())

            }

        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_change_password.setOnClickListener(this)
    }


}
