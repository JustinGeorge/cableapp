package com.justin.cablenetwork.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.justin.cablenetwork.R
import com.justin.cablenetwork.callbacks.FragmentSubscribedClickListener
import com.justin.cablenetwork.fragments.SubscribedChannelsFragment
import com.justin.cablenetwork.models.Channel
import com.justin.cablenetwork.models.SubscribedChannel
import kotlinx.android.synthetic.main.item_subscribed_channel.view.*
import java.text.ParseException
import java.text.SimpleDateFormat

class SubscribedChannelsAdapter(
    mContext: Context?,
    mSubscribedChannels: ArrayList<SubscribedChannel>,
    subscribedChannelsFragment: SubscribedChannelsFragment
) : RecyclerView.Adapter<SubscribedChannelsAdapter.SubscribedChannelViewHolder>() {

    var mContext =mContext
    private var mSubscribedChannel = mSubscribedChannels
    private val mSubscribedListener : FragmentSubscribedClickListener = subscribedChannelsFragment as FragmentSubscribedClickListener


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SubscribedChannelViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_subscribed_channel, parent, false)
        return SubscribedChannelViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mSubscribedChannel.size
    }

    override fun onBindViewHolder(viewHolder : SubscribedChannelViewHolder, position: Int) {
            viewHolder.bind(mContext,mSubscribedChannel.get(position))
            viewHolder.getCheckbox().setOnClickListener {
               // viewHolder.getCheckbox().isChecked = !viewHolder.getCheckbox().isChecked
                val subscribedChannel = mSubscribedChannel.get(position)
                if (viewHolder.getCheckbox().isChecked){
                    mSubscribedListener.onSubscribed(Channel(subscribedChannel.packageId,subscribedChannel.name,
                        subscribedChannel.price))
                    mSubscribedChannel.get(position).isSelected = true
                }else{
                    mSubscribedListener.onUnSubscribed(Channel(subscribedChannel.packageId,subscribedChannel.name,
                        subscribedChannel.price))
                    mSubscribedChannel.get(position).isSelected = false
                }
            }
    }


    class SubscribedChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(context: Context?,subscribedChannel: SubscribedChannel) {
            mChannelName.text = "${subscribedChannel.name}"

            if (subscribedChannel.isActive!!){
                mChannelStatus.text = "Active"
                subscribedChannel.isSelected = true
                context?.resources?.getColor(R.color.colorGreen)?.let { mChannelStatus.setTextColor(it) }
            }else{
                mChannelStatus.text = "Inactive"
                subscribedChannel.isSelected = false
                context?.resources?.getColor(R.color.colorRed)?.let { mChannelStatus.setTextColor(it) }
            }

            mChannelPrice.text = "Price : ${subscribedChannel.price}"
            mChannelStart.text = "${getDateInFormat(subscribedChannel.startDate)} - ${getDateInFormat(subscribedChannel.endDate)}"
            mSubscribed.isChecked = subscribedChannel.isSelected

        }

        fun getCheckbox() : CheckBox {
            return mSubscribed
        }

        private val mChannelName = itemView.txt_channel_name
        private val mChannelStatus = itemView.txt_channel_status
        private val mChannelPrice = itemView.txt_channel_price
        private val mChannelStart = itemView.txt_channel_start_date
        private val mChannelEnd = itemView.txt_channel_end_date
        private val  mSubscribed  = itemView.cb_channel

        private val defaultDateFormat = SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        private val expectedDateFormat = SimpleDateFormat( "yyyy/MMM/dd")

        private fun getDateInFormat(date : String?) : String? {
            try {
                if (date != null) {
                    val defaultDate = defaultDateFormat.parse(date)
                    return expectedDateFormat.format(defaultDate)
                }
            }catch (e: ParseException){

            }
            return null
        }

    }

}