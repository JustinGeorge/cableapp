package com.justin.cablenetwork.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.justin.cablenetwork.R
import com.justin.cablenetwork.models.BillDetail
import com.justin.cablenetwork.models.BranchMaster
import com.justin.cablenetwork.models.CustomerMaster
import com.justin.cablenetwork.models.PackageMaster
import kotlinx.android.synthetic.main.layout_bill_company_details.view.*
import kotlinx.android.synthetic.main.layout_bill_customer_details.view.*
import kotlinx.android.synthetic.main.layout_bill_payment_details.view.*
import kotlinx.android.synthetic.main.layout_bills_channel_details.view.*

class PrintBillAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_COMPANY = 1
    private val TYPE_CUSTOMER = 2
    private val TYPE_CHANNELS = 3
    private val TYPE_PAYMENT = 4

    private val mBranchMaster = ArrayList<BranchMaster>()
    private val mCustomerMaster = ArrayList<CustomerMaster>()
    private val  mBillDetails= ArrayList<BillDetail>()
    private val mPackageMaster = ArrayList<PackageMaster>()

    override fun onCreateViewHolder(viewgroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
         when (viewType) {
            TYPE_COMPANY -> {
                val v = LayoutInflater.from(viewgroup.context).inflate(
                    R.layout.layout_bill_company_details,
                    viewgroup, false
                )
                return CompanyDetailsViewHolder(v)
            }

             TYPE_CUSTOMER -> {
                 val v = LayoutInflater.from(viewgroup.context).inflate(
                     R.layout.layout_bill_customer_details,
                     viewgroup, false
                 )
                 return CustomerDetailsViewHolder(v)
             }

             TYPE_PAYMENT -> {
                 val v = LayoutInflater.from(viewgroup.context).inflate(
                     R.layout.layout_bill_payment_details,
                     viewgroup, false
                 )
                 return PaymentDetailsViewHolder(v)
             }

            else -> {
                val v = LayoutInflater.from(viewgroup.context).inflate(
                    R.layout.layout_bills_channel_details,
                    viewgroup, false
                )
                return ChannelDetailsViewHolder(v)
            }

        }
    }

    override fun getItemCount(): Int {
        if (mBranchMaster.size <=0 ||
                mBillDetails.size <= 0 ||
                mPackageMaster.size <= 0 ||
                mCustomerMaster.size <= 0){
            return 0
        }
        return (mBranchMaster.size + mBillDetails.size + mPackageMaster.size + mCustomerMaster.size)
    }

    override fun getItemViewType(position: Int): Int {

        val packageSize = (mBranchMaster.size + mBillDetails.size + mPackageMaster.size + mCustomerMaster.size)-1

       when(position){
           0 -> {
               return TYPE_COMPANY
           }

           1 -> {
               return TYPE_CUSTOMER
           }

           packageSize -> {
                return TYPE_PAYMENT
           }

           else -> {
               return TYPE_CHANNELS
           }
       }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position : Int) {

        when(getItemViewType(position)){

            TYPE_COMPANY -> {
                (viewHolder as CompanyDetailsViewHolder).bindData(mBranchMaster[0])
            }

            TYPE_CUSTOMER -> {
                (viewHolder as CustomerDetailsViewHolder).bindData(mCustomerMaster[0],mBillDetails[0])
            }

            TYPE_CHANNELS -> {
                Log.d("TAG_JUSTIN","position : $position , Size : ${mPackageMaster.size} : Total : ${(mBranchMaster.size + mBillDetails.size + mPackageMaster.size + mCustomerMaster.size)}")
                (viewHolder as ChannelDetailsViewHolder).bindData(mPackageMaster[position-2])
                Log.d("TAG_JUSTIN",mPackageMaster[position-2].toString())
            }

            TYPE_PAYMENT -> {
                (viewHolder as PaymentDetailsViewHolder).bindData(mBillDetails[0])
            }
        }

    }

    fun setBillData(
        branchMaster: List<BranchMaster>?,
        customerMaster: List<CustomerMaster>?,
        billDetails: List<BillDetail>?,
        packageMaster: List<PackageMaster>?
    ) {

        branchMaster?.let {
            mBranchMaster.clear()
            mBranchMaster.addAll(it) }
        customerMaster?.let {
            mCustomerMaster.clear()
            mCustomerMaster.addAll(it) }
        billDetails?.let {
            mBillDetails.clear()
            mBillDetails.addAll(it) }
        packageMaster?.let {
            mPackageMaster.clear()
            mPackageMaster.addAll(it)
        }
    }


    class CompanyDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bindData(branchMaster: BranchMaster) {
            mCompanyName.text = branchMaster.branchName
            mCompanyPlace.text = branchMaster.city
            mCompanyAddress.text = branchMaster.address
            mCompanyPhone.text = branchMaster.phonenumber
        }

        private val mCompanyName = itemView.txt_company_title
        private val mCompanyPlace = itemView.txt_company_place
        private val mCompanyAddress = itemView.txt_company_address
        private val mCompanyPhone = itemView.txt_company_phone
    }

    class ChannelDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bindData(packageMaster: PackageMaster) {
            mChanelName.text = packageMaster.name
            mChanelPrice.text = packageMaster.price
        }

        private val mChanelName = itemView.txt_channel_name
        private val mChanelPrice = itemView.txt_channel_price
    }

    class CustomerDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(customerMaster: CustomerMaster, billDetail: BillDetail) {
            mCustomerName.text = customerMaster.customerName
            mCustomerNo.text = customerMaster.uniqueCustomerNumber
            mReceiptNo.text = billDetail.receiptNo
            mLastBalance.text = billDetail.lastbalance
            mCurrentBalance.text = billDetail.currentbalance
            mStbNo.text = customerMaster.stbNumber
        }

        private val mCustomerName = itemView.txt_customer_name
        private val mCustomerNo = itemView.txt_customer_no
        private val mReceiptNo = itemView.txt_receipt_no
        private val mLastBalance = itemView.txt_last_balance
        private val mCurrentBalance = itemView.txt_current_balance
        private val mStbNo = itemView.txt_stb_no
    }

    class PaymentDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(billDetail: BillDetail) {
            mTotal.text = billDetail.totalPrice
            mCgst.text = billDetail.cGST
            mSgst.text = billDetail.sGST
            mGtotal.text = billDetail.grandTotal
        }

        private val mTotal = itemView.txt_total
        private val mCgst = itemView.txt_cgst
        private val mSgst = itemView.txt_sgst
        private val mGtotal = itemView.txt_g_total
    }

}


