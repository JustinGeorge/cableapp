package com.justin.cablenetwork.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.justin.cablenetwork.models.ItemUserListModel
import com.justin.cablenetwork.R
import kotlinx.android.synthetic.main.item_user_info.view.*

class UserListAdapter(
    context : Context,
    mUserList: ArrayList<ItemUserListModel>
) : RecyclerView.Adapter<UserListAdapter.PaymentItemViewHolder>() {

    private val mContext : Context = context
    private val mUserList : ArrayList<ItemUserListModel> = mUserList
    private var mUserItemClickListener : UserItemClickListener

    init {
        this.mUserItemClickListener = mContext as UserItemClickListener
    }


    interface UserItemClickListener{
       fun  onUserItemClick(itemUserListModel : ItemUserListModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PaymentItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_user_info, parent, false)
        return PaymentItemViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mUserList.size
    }

    override fun onBindViewHolder(viewHolder: PaymentItemViewHolder, position: Int) {
        viewHolder.bind(mUserList.get(position), mUserItemClickListener)
    }


    class PaymentItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(
            data: ItemUserListModel,
            mUserItemClickListener: UserItemClickListener
        ) {
            mUserName.text = data.customerName
            mUserMobile.text = data.mobile
            mSbNumber.text = data.stbNumber
            mUserLocation.text = data.location
            itemView.setOnClickListener { mUserItemClickListener.onUserItemClick(data) }
        }

        private val mUserName = itemView.txt_name
        private val mUserMobile = itemView.txt_phone
        private val mSbNumber = itemView.txt_setup_box
        private val mUserLocation = itemView.txt_location

    }
}