package com.justin.cablenetwork.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import com.justin.cablenetwork.R
import com.justin.cablenetwork.models.PaymentHistory
import kotlinx.android.synthetic.main.payment_cell_layout.view.*
import kotlinx.android.synthetic.main.payment_cell_title_layout.view.*


class UserPaymentHistoryAdapter(val context: Context,
                                val parent: TableLayout,
                                val mHeaderList: List<String>,
                                val mItemList: List<PaymentHistory>) {


     fun setAdapter(){
        val header = onCreateHeader(parent)
        header.bind(mHeaderList)
        parent.addView(header.getView())

        for ( item in mItemList) {
            val cellItem = onCreateItem(parent)
            cellItem.bind(item)
            parent.addView(cellItem.getView())
        }

    }


   private fun onCreateHeader(parent: ViewGroup): HistoryHeaderItemHolder{
        val v = LayoutInflater.from(parent.context).inflate(R.layout.payment_cell_title_layout, parent, false)
        return HistoryHeaderItemHolder(v)
    }

    private fun onCreateItem(parent: ViewGroup): HistoryItemHolder{
        val v = LayoutInflater.from(parent.context).inflate(R.layout.payment_cell_layout, parent, false)
        return HistoryItemHolder(v)
    }


    class HistoryHeaderItemHolder(itemView: View) {


        fun bind(data: List<String>) {
            mYear.text = data[0]
            mMonth.text = data[1]
            mMonthName.text = data[2]
            mSalesCount.text = data[3]

        }

        fun getView(): View{
            return headerView
        }

        private val headerView = itemView
        private val mYear = itemView.tit_year
        private val mMonth = itemView.tit_month
        private val mMonthName = itemView.tit_month_name
        private val mSalesCount = itemView.tit_sales_count

    }

    class HistoryItemHolder(itemView: View) {


        fun bind(
            data: PaymentHistory
        ) {
            mYear.text = data.year.toString()
            mMonth.text = data.month.toString()
            mMonthName.text = data.monthName.toString()
            mSalesCount.text = data.sumAmount.toString()

        }

        fun getView(): View{
            return itemView
        }

        private val itemView = itemView

        private val mYear = itemView.txt_year
        private val mMonth = itemView.txt_month
        private val mMonthName = itemView.txt_month_name
        private val mSalesCount = itemView.txt_sales_count

    }


}