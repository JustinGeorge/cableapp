package com.justin.cablenetwork.adapters

import android.content.Context
import android.view.ViewGroup
import com.evrencoskun.tableview.adapter.AbstractTableAdapter
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder
import android.R
import android.graphics.Color
import android.view.View

import android.view.LayoutInflater
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.item_tbl_cell.view.*




class MyTableViewAdapter(context: Context?) : AbstractTableAdapter<String, String, String>(context) {

    var  sContext : Context = context!!


    class MyColumnHeaderViewHolder(itemView: View) : AbstractViewHolder(itemView) {

         val mChannelName = itemView.txt_history
    }

    override fun onCreateColumnHeaderViewHolder(parent: ViewGroup?, viewType: Int): AbstractViewHolder {
        /*val layout = LayoutInflater.from(sContext).inflate(
            R.layout.item_tbl, parent, false
        )*/
        val layout = LayoutInflater.from(parent?.context).
            inflate(com.justin.cablenetwork.R.layout.item_tbl_cell, parent, false)

        // Create a ColumnHeader ViewHolder
        return MyColumnHeaderViewHolder(layout)
    }

    override fun onBindColumnHeaderViewHolder(
        holder: AbstractViewHolder?,
        columnHeaderItemModel: Any?,
        columnPosition: Int
    ) {
        val texts = columnHeaderItemModel as String
        val columnHeaderViewHolder = holder as MyColumnHeaderViewHolder
        columnHeaderViewHolder.mChannelName.text = texts
    }



    override fun onBindRowHeaderViewHolder(holder: AbstractViewHolder?, rowHeaderItemModel: Any?, rowPosition: Int) {
        val texts = rowHeaderItemModel as String
        val columnHeaderViewHolder = holder as MyColumnHeaderViewHolder
        columnHeaderViewHolder.mChannelName.text  =texts
    }

    override fun onCreateRowHeaderViewHolder(parent: ViewGroup?, viewType: Int): AbstractViewHolder {
        val layout = LayoutInflater.from(parent?.context).
            inflate(com.justin.cablenetwork.R.layout.item_tbl_cell, parent, false)

        // Create a ColumnHeader ViewHolder
        return MyColumnHeaderViewHolder(layout)
    }

    override fun onCreateCellViewHolder(parent: ViewGroup?, viewType: Int): AbstractViewHolder {
        val layout = LayoutInflater.from(parent?.context).
            inflate(com.justin.cablenetwork.R.layout.item_tbl_cell, parent, false)

        // Create a ColumnHeader ViewHolder
        return MyColumnHeaderViewHolder(layout)
    }

    override fun onCreateCornerView(): View {
        val view  = View(sContext)
        //view.layoutParams = LinearLayout.LayoutParams(500,150)
        view.setBackgroundColor(Color.MAGENTA)

        // Create a ColumnHeader ViewHolder
        return view
    }

    override fun onBindCellViewHolder(
        holder: AbstractViewHolder?,
        cellItemModel: Any?,
        columnPosition: Int,
        rowPosition: Int
    ) {
        val texts = cellItemModel as String
        val columnHeaderViewHolder = holder as MyColumnHeaderViewHolder
        columnHeaderViewHolder.mChannelName.text  = texts
    }

    override fun getColumnHeaderItemViewType(position: Int): Int {
        return 0
    }

    override fun getRowHeaderItemViewType(position: Int): Int {
        return 0
    }

    override fun getCellItemViewType(position: Int): Int {
        return 0
    }


}
