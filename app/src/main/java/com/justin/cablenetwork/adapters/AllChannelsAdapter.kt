package com.justin.cablenetwork.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.justin.cablenetwork.R
import com.justin.cablenetwork.callbacks.FragmentSubscribedClickListener
import com.justin.cablenetwork.fragments.AllChannelListFragment
import com.justin.cablenetwork.models.Channel
import com.justin.cablenetwork.models.ChannelInfo
import kotlinx.android.synthetic.main.item_channel.view.*
import kotlinx.android.synthetic.main.item_subscribed_channel.view.cb_channel
import kotlinx.android.synthetic.main.item_subscribed_channel.view.txt_channel_name
import kotlinx.android.synthetic.main.item_subscribed_channel.view.txt_channel_price

class AllChannelsAdapter(
    mContext: Context?,
    mAllChannels: ArrayList<ChannelInfo>,
    allChannelListFragment: AllChannelListFragment
) : RecyclerView.Adapter<AllChannelsAdapter.SubscribedChannelViewHolder>() {

    var mContext =mContext
    private var mAllChannels = mAllChannels
    private val mSubscribedListener : FragmentSubscribedClickListener = allChannelListFragment as FragmentSubscribedClickListener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SubscribedChannelViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_channel, parent, false)
        return SubscribedChannelViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mAllChannels.size
    }

    override fun onBindViewHolder(viewHolder : SubscribedChannelViewHolder, position: Int) {
            viewHolder.bind(mContext,mAllChannels.get(position))

        viewHolder.getCheckbox().setOnClickListener {
            //viewHolder.getCheckbox().isChecked = !viewHolder.getCheckbox().isChecked
            val subscribedChannel = mAllChannels.get(position)
            if (viewHolder.getCheckbox().isChecked){
                mSubscribedListener.onSubscribed(Channel(subscribedChannel.id,subscribedChannel.name,subscribedChannel.price))
                mAllChannels.get(position).isSelected = true
            }else{
                mSubscribedListener.onUnSubscribed(subscribedChannel.id)
                mAllChannels.get(position).isSelected = false
            }
        }

    }


    class SubscribedChannelViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context?,channel: ChannelInfo) {
            mChannelName.text = channel.name
            mChannelPrice.text = "Price : ${channel.price}"
            mChannelGenre.text = channel.genre
            mChannelLan.text = channel.language
            mSubscribed.isChecked = channel.isSelected
        }

        fun getCheckbox() : CheckBox {
            return mSubscribed
        }

        private val mChannelName = itemView.txt_channel_name
        private val mChannelPrice = itemView.txt_channel_price
        private val mChannelGenre = itemView.txt_channel_genre
        private val mChannelLan = itemView.txt_channel_language
        private val  mSubscribed  = itemView.cb_channel

    }

}