package com.justin.cablenetwork.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.justin.cablenetwork.R
import com.justin.cablenetwork.utils.ItemOffsetDecoration
import kotlinx.android.synthetic.main.activity_user_search.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class UserSearchActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
           R.id.im_search -> {
               startActivity(Intent(this@UserSearchActivity, PaymentActivity::class.java))
               overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
           }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_search)
        initView()
        initRecycler()
    }

    private fun initRecycler() {
        recy_user_list.layoutManager = LinearLayoutManager(this)
        recy_user_list.addItemDecoration(ItemOffsetDecoration(this,R.dimen.item_offset))
        //recy_user_list.adapter = UserListAdapter(this, mUserList)
    }

    private fun initView() {
        txt_title_toolbar.text = "Search User"
        setSupportActionBar(findViewById(R.id.include))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        im_search.setOnClickListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
