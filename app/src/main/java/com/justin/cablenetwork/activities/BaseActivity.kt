package com.justin.cablenetwork.activities

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.justin.cablenetwork.R

open class BaseActivity : AppCompatActivity() {

    lateinit var builder: AlertDialog.Builder
    var mSimpleProgressDialog: ProgressDialog? = null


    fun showSimpleProgressAlert(context: Context) {
        // builder = AlertDialog.Builder(context)
        if (mSimpleProgressDialog != null && mSimpleProgressDialog!!.isShowing) {
            mSimpleProgressDialog?.hide()
        } else {
            mSimpleProgressDialog = ProgressDialog(context, R.style.CustomProgressBarTheme)
        }
        mSimpleProgressDialog?.setMessage("Please wait!")

        mSimpleProgressDialog?.show()

    }

    fun hideSimpleProgressAlert() {
        if (mSimpleProgressDialog != null && mSimpleProgressDialog!!.isShowing) {
            mSimpleProgressDialog?.hide()
        }
    }


    private val MY_PERMISSIONS_REQUEST_READ_CONTACTS: Int = 1001

    fun checkAppWritePermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_CONTACTS -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(this,"Permission Granted!",Toast.LENGTH_SHORT).show()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this,"Permission not Granted!",Toast.LENGTH_SHORT).show()
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }




}

