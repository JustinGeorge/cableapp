package com.justin.cablenetwork.activities

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.MenuItem
import com.justin.cablenetwork.R
import com.justin.cablenetwork.adapters.UserPaymentHistoryAdapter
import com.justin.cablenetwork.models.ItemUserListModel
import com.justin.cablenetwork.models.PaymentHistory
import com.justin.cablenetwork.models.PaymentHistoryResponse
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import com.justin.cablenetwork.utils.NetworkConnectionUtils
import kotlinx.android.synthetic.main.activity_home_acivity.*
import kotlinx.android.synthetic.main.activity_user_payment_history.*
import kotlinx.android.synthetic.main.fragment_channel_list.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserPaymentHistoryActivity : BaseActivity() {

    val mHeader =  object : ArrayList<String>() {
        init {
            add("Year")
            add("Month")
            add("Month Name")
            add("Bill Amount")
        }
    }

    val mItemList =  object : ArrayList<PaymentHistory>() {
        init {
            add( PaymentHistory(2012,1, "January",23))
            add( PaymentHistory(2012,2, "January",23))
            add( PaymentHistory(2012,3, "January",23))
            add( PaymentHistory(2012,4, "January",23))
            add( PaymentHistory(2012,5, "January",23))
            add( PaymentHistory(2012,6, "January",23))
            add( PaymentHistory(2012,7, "January",23))
            add( PaymentHistory(2012,8, "January",23))
            add( PaymentHistory(2012,9, "January",23))
            add( PaymentHistory(2012,10, "January",23))
            add( PaymentHistory(2012,11, "January",23))
            add( PaymentHistory(2012,12, "January",23))
            add( PaymentHistory(2012,13, "January",23))
            add( PaymentHistory(2012,14, "January",23))
            add( PaymentHistory(2012,15, "January",23))
            add( PaymentHistory(2012,16, "January",23))
            add( PaymentHistory(2012,17, "January",23))
            add( PaymentHistory(2012,18, "January",23))
            add( PaymentHistory(2012,19, "January",23))
            add( PaymentHistory(2012,20, "January",23))
        }
    }

    var mPaymentHistoryList  =  ArrayList<PaymentHistory>()

    private lateinit var userInfo: ItemUserListModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_payment_history)
        getData(intent.extras)
        initView()
        getUserPaymentDetails()
    }

    private fun getData(extras: Bundle?) {
        userInfo = extras?.get("param_user_info") as ItemUserListModel
    }

    private fun initView() {
        setupToolbar()
    }

    private fun setupToolbar() {
        txt_title_toolbar.text = getString(R.string.payment_history)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getUserPaymentDetails(){

        if (!NetworkConnectionUtils.getConnectionStatus(this@UserPaymentHistoryActivity)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        showSimpleProgressAlert(this@UserPaymentHistoryActivity)

        Log.d("TAG_CALL","All fragment Token : " +
                "${SharedPreferenceManager.getInstance(this@UserPaymentHistoryActivity).getString(
            PreferenceUtils.LoginConstants.token)} , id ${userInfo.uniqueId!!}")

        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getFullPaymentHistoryByCustId(
            SharedPreferenceManager.getInstance(this@UserPaymentHistoryActivity).getString(
                PreferenceUtils.LoginConstants.token),
            userInfo.uniqueId!!
        )

        call?.enqueue(object : Callback<PaymentHistoryResponse> {
            override fun onResponse(
                call: Call<PaymentHistoryResponse>,
                response: Response<PaymentHistoryResponse>
            ) {

                hideSimpleProgressAlert()
                val paymentHistoryResponse = response.body()

                if (paymentHistoryResponse?.success!!) {
                    mPaymentHistoryList.addAll(paymentHistoryResponse?.data!!)
                    val userPaymentAdapter = UserPaymentHistoryAdapter(this@UserPaymentHistoryActivity,
                        table_payment,mHeader,mPaymentHistoryList)
                    userPaymentAdapter.setAdapter()
                } else {

                    Snackbar.make(
                        table_payment,
                        paymentHistoryResponse?.message!!,
                        Snackbar.LENGTH_LONG
                    )
                        .show()
                }

            }

            override fun onFailure(call: Call<PaymentHistoryResponse>, t: Throwable) {
                /*isLoading = false
                if (PAGE_NUMBER == 0) {

                    setNoDataVisibility(true)
                }*/

                Snackbar.make(
                    table_payment,
                    "No data found!",
                    Snackbar.LENGTH_SHORT
                )
                    .show()

                hideSimpleProgressAlert()
            }

        })

    }


}
