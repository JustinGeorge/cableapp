package com.justin.cablenetwork.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import com.justin.cablenetwork.R

class LauncherActivity : AppCompatActivity() {

    private val TIME_IN_MILLS = 3000L
    private var mHandler = Handler()
    private  var mRunnable = object : Runnable {
        override fun run() {
            openActivity()
        }
    }

    private fun openActivity() {
        if (SharedPreferenceManager.getInstance(this@LauncherActivity)
                .getBoolean(PreferenceUtils.LoginConstants.user_login)){
            openHomeActivity()
        }else{
            openLoginActivity()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)

    }

    override fun onStart() {
        super.onStart()
        mHandler.postDelayed(mRunnable,TIME_IN_MILLS)
    }

    override fun onStop() {
        super.onStop()
        mHandler.removeCallbacks(mRunnable)
    }

    private fun openHomeActivity(){
            val intent = Intent(this@LauncherActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
            overridePendingTransition(
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
    }

    private fun openLoginActivity(){
        val intent = Intent(this@LauncherActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )
    }

}
