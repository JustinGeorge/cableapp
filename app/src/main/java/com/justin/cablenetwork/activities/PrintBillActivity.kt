package com.justin.cablenetwork.activities

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.justin.cablenetwork.R
import com.justin.cablenetwork.adapters.PrintBillAdapter
import com.justin.cablenetwork.adapters.UserPaymentHistoryAdapter
import com.justin.cablenetwork.bluetoothprinter.AppGlobals
import com.justin.cablenetwork.bluetoothprinter.BluetoothPrinterHelper
import com.justin.cablenetwork.dialogs.AddPrinterDialogListener
import com.justin.cablenetwork.dialogs.Dialogs
import com.justin.cablenetwork.models.*
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import com.justin.cablenetwork.utils.NetworkConnectionUtils
import com.ngx.BluetoothPrinter
import kotlinx.android.synthetic.main.activity_home_acivity.*
import kotlinx.android.synthetic.main.activity_print_bill.*
import kotlinx.android.synthetic.main.activity_print_bill.txt_cancel
import kotlinx.android.synthetic.main.activity_user_payment_history.*
import kotlinx.android.synthetic.main.dialog_channel_list.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class PrintBillActivity : BaseActivity() ,AddPrinterDialogListener, View.OnClickListener {


    private val TAG = "TAG_PRINTER"

    private var mBluetoothStatus: String? = ""
    private var mTitleConnecting = "connecting..."
    private var mTitleConnectedTo = "connected: "
    private var mTitleNotConnected = "not connected"
    private var mConnectedDeviceName: String? = ""
    private var bluetoothStatus: String? = ""

    private val mBpInstance = BluetoothPrinter.INSTANCE
    private val mDialogs = Dialogs()

    private lateinit var userInfo: ItemUserListModel
    private lateinit var mPrintBillAdapter : PrintBillAdapter

    private var mBranchMaster = ArrayList<BranchMaster>()
    private var mCustomerMaster = ArrayList<CustomerMaster>()
    private var  mBillDetails= ArrayList<BillDetail>()
    private var mPackageMaster = ArrayList<PackageMaster>()

    private val mBpStatusHandler = @SuppressLint("HandlerLeak") object : Handler(){
        override fun handleMessage(msg: Message?) {
            Log.d(TAG,"mBpStatusHandler  message :  ${msg?.what} , message argument : ${msg?.arg1}")
            when(msg?.what){
                BluetoothPrinter.MESSAGE_STATE_CHANGE -> {
                    when (msg.arg1) {
                        BluetoothPrinter.STATE_CONNECTED -> {
                            mBluetoothStatus = "$mTitleConnectedTo  :  $mConnectedDeviceName"
                        }
                        BluetoothPrinter.STATE_CONNECTING -> {
                            mBluetoothStatus = mTitleConnecting
                        }
                        BluetoothPrinter.STATE_LISTEN, BluetoothPrinter.STATE_NONE -> {
                            mBluetoothStatus = mTitleNotConnected
                        }
                    }
                }

                BluetoothPrinter.MESSAGE_DEVICE_NAME -> {
                    mConnectedDeviceName = msg.data.getString(
                        BluetoothPrinter.DEVICE_NAME
                    )
                }

                BluetoothPrinter.MESSAGE_STATUS -> {
                    bluetoothStatus = msg.data.getString(BluetoothPrinter.STATUS_TEXT)
                }

            }
        }
    }

    private val mBluetoothReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            when(action){
                BluetoothDevice.ACTION_FOUND -> {
                    Log.d(TAG,"mBluetoothReceiver : BluetoothDevice.ACTION_FOUND")
                }
                BluetoothDevice.ACTION_ACL_CONNECTED -> {
                    Log.d(TAG,"mBluetoothReceiver : BluetoothDevice.ACTION_ACL_CONNECTED")
                    BluetoothPrinterHelper.SetIsConnected(true)
                    setPrinterStatus(resources.getString(R.string.connected),true)
                    //AppGlobals.printerTestDialogFragment.SetPrinterStatus()
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    Log.d(TAG,"mBluetoothReceiver : BluetoothAdapter.ACTION_DISCOVERY_FINISHED")
                }
                BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED -> {
                    Log.d(TAG,"mBluetoothReceiver : BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED")
                }
                BluetoothDevice.ACTION_ACL_DISCONNECTED -> {
                    Log.d(TAG,"mBluetoothReceiver :  BluetoothDevice.ACTION_ACL_DISCONNECTED")
                    setPrinterStatus(resources.getString(R.string.disconnected),false)
                    BluetoothPrinterHelper.CloseConnection()
                    BluetoothPrinterHelper.SetIsConnected(false)
                    //AppGlobals.printerTestDialogFragment.SetPrinterStatus()
                }
            }

        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerBluetoothReceiver()
        setContentView(R.layout.activity_print_bill)
        getData(intent.extras)
        initView()
        AppGlobals.MainActivity = this
        initializeBluetoothPrinter()
        getBlueToothPrinterName()
        initializeRecyclerView()
        getUserBill()
    }

    private fun getData(extras: Bundle?) {
        userInfo = extras?.get("param_user_info") as ItemUserListModel
    }


    private fun initializeRecyclerView() {
        mPrintBillAdapter = PrintBillAdapter()
        recy_bill.layoutManager = LinearLayoutManager(this@PrintBillActivity)
        recy_bill.adapter = mPrintBillAdapter
    }

    private fun getBlueToothPrinterName() {3
       val printerName = SharedPreferenceManager.getInstance(this@PrintBillActivity)
            .getString(PreferenceUtils.PrintBillsConstants.bluetoothprintername)
        if (printerName.isNotEmpty()){
            txt_printer_name.text = printerName
            AppGlobals.PrinterName = printerName
            connectToPrinter()
        }else{
            txt_printer_name.text = resources.getString(R.string.printer_not_found)
            Toast.makeText(this@PrintBillActivity,"Please add a printer!", Toast.LENGTH_SHORT).show()
        }
        setPrinterStatus(resources.getString(R.string.disconnected),false)
    }

    private fun setPrinterStatus(status : String, connection : Boolean){
        txt_printer_status.text = status
        if (connection){
            txt_printer_status.setTextColor(resources.getColor(R.color.colorGreen))
        }else{
            txt_printer_status.setTextColor(resources.getColor(R.color.colorRed))
        }

    }

    private fun connectToPrinter() {
        BluetoothPrinterHelper.ConnectToPrinter()
    }

    private fun initView() {
        setupToolbar()
        im_printer_add.setOnClickListener(this)
        txt_print.setOnClickListener(this)
        txt_cancel.setOnClickListener(this)
    }

    private fun setupToolbar() {
        txt_title_toolbar.text = getString(R.string.print_bill)
        setSupportActionBar(findViewById(R.id.include))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.im_printer_add -> {
             mDialogs.showAddPrinterAlertDialog(this@PrintBillActivity)
            }

            R.id.txt_print -> {
                if (mBranchMaster?.size!! <= 0 ||
                    mCustomerMaster?.size!! <= 0 ||
                    mBillDetails?.size!! <= 0 ||
                    mPackageMaster.size!! <= 0){
                    Toast.makeText(this@PrintBillActivity,"Can't print bill. Some data is missing!",Toast.LENGTH_LONG).show()
                    return
                }
                BluetoothPrinterHelper.printBill(mBranchMaster,mCustomerMaster,mBillDetails,mPackageMaster)
            }

            R.id.txt_cancel -> {
                gotoHomeActivity()
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        gotoHomeActivity()
    }


    private fun gotoHomeActivity() {
        val homeIntent = Intent(this@PrintBillActivity, HomeActivity::class.java)
        startActivity(homeIntent)
    }


    override fun onAddClicked(name: String) {
        if (name.isNotEmpty()){
            Toast.makeText(this@PrintBillActivity,"Name : $name", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "Name : $name")
            SharedPreferenceManager.getInstance(this@PrintBillActivity)
                .putString(PreferenceUtils.PrintBillsConstants.bluetoothprintername,name)
            getBlueToothPrinterName()
        }else{
            Toast.makeText(this@PrintBillActivity,"Please enter a valid printer name!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_print_bills, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> {
                gotoHomeActivity()
            }
            R.id.action_connect_device -> {
                getBlueToothPrinterName()
            }
            R.id.action_refresh_bill -> {
                getUserBill()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        mBpInstance.onActivityResume()

    }

    private fun getUserBill() {
        if (!NetworkConnectionUtils.getConnectionStatus(this@PrintBillActivity)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        showSimpleProgressAlert(this@PrintBillActivity)

        Log.d("TAG_CALL","All fragment Token : " +
                "${SharedPreferenceManager.getInstance(this@PrintBillActivity).getString(
                    PreferenceUtils.LoginConstants.token)} , id ${userInfo.uniqueId!!}")

        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getPrintBillDetails(
            SharedPreferenceManager.getInstance(this@PrintBillActivity).getString(
                PreferenceUtils.LoginConstants.token),
            userInfo.uniqueId!!
        )

        call?.enqueue(object : Callback<PrintBillResponse> {
            override fun onResponse(
                call: Call<PrintBillResponse>,
                response: Response<PrintBillResponse>
            ) {

                hideSimpleProgressAlert()
                val paymentHistoryResponse = response.body()

                if (paymentHistoryResponse?.success!!) {
                    //mPaymentHistoryList.addAll(paymentHistoryResponse?.data!!)

                    //TODO Add the code to show the bill
                    Log.d("TAG_JUSTIN","PrintBill success")

                    if (paymentHistoryResponse.branchMaster?.size!! <= 0 ||
                        paymentHistoryResponse.customerMaster?.size!! <= 0 ||
                        paymentHistoryResponse.billDetails?.size!! <= 0 ||
                        paymentHistoryResponse.packageMaster?.size!! <= 0){
                        Toast.makeText(this@PrintBillActivity,"Some data is missing!",Toast.LENGTH_LONG).show()
                        return
                    }

                    paymentHistoryResponse.branchMaster?.let {
                        mBranchMaster.clear()
                        mBranchMaster.addAll(it) }
                    paymentHistoryResponse.customerMaster?.let {
                        mCustomerMaster.clear()
                        mCustomerMaster.addAll(it) }
                    paymentHistoryResponse.billDetails?.let {
                        mBillDetails.clear()
                        mBillDetails.addAll(it) }
                    paymentHistoryResponse.packageMaster?.let {
                        mPackageMaster.clear()
                        mPackageMaster.addAll(it) }

                    mPrintBillAdapter.setBillData(
                        mBranchMaster,
                        mCustomerMaster,
                        mBillDetails,
                        mPackageMaster)
                    mPrintBillAdapter.notifyDataSetChanged()

                } else {

                    Snackbar.make(
                        recy_bill,
                        paymentHistoryResponse?.message!!,
                        Snackbar.LENGTH_LONG
                    )
                        .show()
                }

            }

            override fun onFailure(call: Call<PrintBillResponse>, t: Throwable) {
                /*isLoading = false
                if (PAGE_NUMBER == 0) {

                    setNoDataVisibility(true)
                }*/

                Snackbar.make(
                    recy_bill,
                    "No data found!",
                    Snackbar.LENGTH_SHORT
                )
                    .show()

                hideSimpleProgressAlert()
            }

        })
    }

    override fun onPause() {
        super.onPause()
        mBpInstance.onActivityPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        BluetoothPrinterHelper.CloseConnection()
        mBpInstance.onActivityDestroy()
        unregisterReceiver(mBluetoothReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mBpInstance.onActivityResult(requestCode, resultCode, data, this);
    }



    private fun registerBluetoothReceiver() {
        Log.d(TAG,"registerBluetoothReceiver")
        val filter1 = IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED)
        val filter2 = IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        val filter3 = IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        this.registerReceiver(mBluetoothReceiver, filter1)
        this.registerReceiver(mBluetoothReceiver, filter2)
        this.registerReceiver(mBluetoothReceiver, filter3)
    }

    private fun initializeBluetoothPrinter() {
        try {
            Log.d(TAG,"initializeBluetoothPrinter")
            mBpInstance.setDebugService(true)
            mBpInstance.initService(this@PrintBillActivity, mBpStatusHandler)
        }catch (e : Exception){
            e.printStackTrace()
        }
    }

}
