package com.justin.cablenetwork.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.justin.cablenetwork.R
import com.justin.cablenetwork.models.ItemUserListModel
import com.justin.cablenetwork.models.PaidAmountResponse
import com.justin.cablenetwork.models.TotalAmountResponse
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import com.justin.cablenetwork.utils.NetworkConnectionUtils
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PaymentActivity : BaseActivity(), View.OnClickListener {


    private lateinit var userInfo: ItemUserListModel
    private var mBillAmount : Float = 0.0F
    private var mPaymentAmount : Float = 0F


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        getData(intent.extras)
        initView()
    }

    private fun getData(extras: Bundle?) {
            userInfo = extras?.get("param_user_info") as ItemUserListModel
    }

    private fun initView() {
        txt_title_toolbar.text = getString(R.string.payment)
        setSupportActionBar(findViewById(R.id.include))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        txt_pay.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.txt_pay -> doPayment()
        }
    }

    private fun doPayment() {
        val payment = edtxt_amount_paying.text.trim().toString()
        mPaymentAmount = if (payment.isNotEmpty()) {
            payment.toFloat()
        }else{
            0f
        }

        if (!mBillAmount.equals(0f) && !mPaymentAmount.equals(0f)){

            callUpdatePaymentApi(mBillAmount,mPaymentAmount)

        }else{
            if (mBillAmount.equals(0f)){
                Toast.makeText(this@PaymentActivity, "Invalid Bill amount!", Toast.LENGTH_SHORT).show()
            }else if(mPaymentAmount.equals(0f)){
                Toast.makeText(this@PaymentActivity, "Please enter a valid amount!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun callUpdatePaymentApi(mBillAmount: Float, mPaymentAmount: Float) {
        if (!NetworkConnectionUtils.getConnectionStatus(this@PaymentActivity)) {
            Snackbar.make(
                rel_container,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        showSimpleProgressAlert(this@PaymentActivity)
        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getSavePaymentHistory(
            SharedPreferenceManager.getInstance(this@PaymentActivity).getString(PreferenceUtils.LoginConstants.token),
            userInfo.uniqueId!!,
            mBillAmount,
            mPaymentAmount

        )

        call?.enqueue(object : Callback<PaidAmountResponse> {

            override fun onFailure(call: Call<PaidAmountResponse>, t: Throwable) {
                hideSimpleProgressAlert()
                Toast.makeText(this@PaymentActivity, getString(R.string.error_something_went_wrong), Toast.LENGTH_SHORT).show()

            }

            override fun onResponse(call: Call<PaidAmountResponse>, response: Response<PaidAmountResponse>) {
                hideSimpleProgressAlert()
                if (response.body()?.success!!) {
                    Toast.makeText(this@PaymentActivity, "Successfully updated payment!", Toast.LENGTH_SHORT).show()
                    getToPrintScreen()
                }else{
                    Toast.makeText(this@PaymentActivity, getString(R.string.error_something_went_wrong), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun getToPrintScreen() {
        val printActivityIntent =  Intent(this@PaymentActivity,PrintBillActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("param_user_info",userInfo)
        printActivityIntent.putExtras(bundle)
        startActivity(printActivityIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        callTotalAmountApi()
    }

    private fun callTotalAmountApi() {
        if (!NetworkConnectionUtils.getConnectionStatus(this@PaymentActivity)) {
            Snackbar.make(
                rel_container,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        showSimpleProgressAlert(this@PaymentActivity)
        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getGetPendingBillBycustId(
            SharedPreferenceManager.getInstance(this@PaymentActivity).getString(PreferenceUtils.LoginConstants.token),
            userInfo.uniqueId!!
        )

        call?.enqueue(object : Callback<TotalAmountResponse> {

            override fun onFailure(call: Call<TotalAmountResponse>, t: Throwable) {
                hideSimpleProgressAlert()
                Toast.makeText(this@PaymentActivity, getString(R.string.error_something_went_wrong), Toast.LENGTH_SHORT).show()

            }

            override fun onResponse(call: Call<TotalAmountResponse>, response: Response<TotalAmountResponse>) {
                hideSimpleProgressAlert()
                if (response.body()?.success!!) {
                    getUserListModels(response.body()!!)
                }else{
                    Toast.makeText(this@PaymentActivity, getString(R.string.error_something_went_wrong), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun getUserListModels(body: TotalAmountResponse) {
        val amount  = body.data?.get(0)?.price
        mBillAmount = if(amount?.isNotEmpty()!!){
            amount.toFloat()
        }else{
            0f
        }
        txt_total_amount.text = "$amount"
    }

}
