package com.justin.cablenetwork.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.justin.cablenetwork.fragments.ChangePasswordFragment
import com.justin.cablenetwork.R
import kotlinx.android.synthetic.main.layout_toolbar.*

class PasswordActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password)
        initView()
        addFragmentToView()
    }

    private fun addFragmentToView() {
        val mChangePasswordFragment = ChangePasswordFragment()
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fra_password_manage,mChangePasswordFragment)
        transaction.commit()
    }

    private fun initView() {
        txt_title_toolbar.text = "Change password"
        setSupportActionBar(findViewById(R.id.include))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
