package com.justin.cablenetwork.activities

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.justin.cablenetwork.*
import com.justin.cablenetwork.models.LoginModel
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.support.design.widget.Snackbar
import com.justin.cablenetwork.utils.LogWriter
import com.justin.cablenetwork.utils.NetworkConnectionUtils


class LoginActivity : BaseActivity(), View.OnClickListener {

    private lateinit var mSharedPreferenceManager: SharedPreferenceManager
    private lateinit var mLoginModel: LoginModel

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txt_sign_in -> {
                if (NetworkConnectionUtils.getConnectionStatus(this@LoginActivity)) {
                    checkLoginDetails()
                } else {
                    Snackbar.make(
                        constraint_layout_login,
                        resources.getString(R.string.no_connection),
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun checkLoginDetails() {
        val mobileNumber = edtxt_mobileNumber.text.toString()
        val password = edtxt_password.text.toString()

        if (TextUtils.isEmpty(mobileNumber) || !TextUtils.isDigitsOnly(mobileNumber)) {
            edtxt_mobileNumber.error = "Enter valid mobile number!"
            return
        } else if (password.isEmpty()) {
            edtxt_password.error = "Enter valid passsword!"
        }

        userLogin(mobileNumber, password)

    }

    private fun userLogin(mobileNumber: String, password: String) {
        showSimpleProgressAlert(this@LoginActivity)
        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getLoginRsponse(mobileNumber, password)
        call?.enqueue(object : Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                hideSimpleProgressAlert()
                LogWriter.getInstance().Log("Login Failed response : "+t.message +"\n"+t.localizedMessage)
                Toast.makeText(this@LoginActivity, "Login Failed ", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                hideSimpleProgressAlert()
                if (response.body()?.success!!) {
                    // Toast.makeText(this@LoginActivity,"Login Success",Toast.LENGTH_SHORT).show()
                    saveLoginInfo(response.body()!!)
                    LogWriter.getInstance().Log("Login Success response : "+response.body()!!.toString())
                }
            }
        })

    }



    private fun gotoHomeActivity() {
        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )
    }

    private fun saveLoginInfo(body: LoginModel) {
        mSharedPreferenceManager.putString(PreferenceUtils.LoginConstants.token, body.token!!)
        mSharedPreferenceManager.putBoolean(PreferenceUtils.LoginConstants.user_login, true)
        val loginDataModel = body.data?.get(0)
        mSharedPreferenceManager.putString(PreferenceUtils.LoginConstants.mobile, loginDataModel?.mobile!!)
        mSharedPreferenceManager.putString(PreferenceUtils.LoginConstants.First_name, loginDataModel?.firstName!!)
        gotoHomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initView()

    }

    private fun initView() {
        txt_sign_in.setOnClickListener(this)
        mSharedPreferenceManager = SharedPreferenceManager.getInstance(this@LoginActivity)
    }

    private fun gotoChangePasswordActivity() {
        val intent = Intent(this@LoginActivity, PasswordActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )
    }


    override fun onResume() {
        super.onResume()
        checkAppWritePermission()
    }

}
