package com.justin.cablenetwork.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.justin.cablenetwork.R
import com.justin.cablenetwork.adapters.MyTableViewAdapter
import kotlinx.android.synthetic.main.activity_payment_history.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import android.provider.DocumentsContract.Document.COLUMN_SIZE
import android.util.Log
import java.util.*
import kotlin.collections.ArrayList
import android.icu.lang.UCharacter.JoiningGroup.SAD
import android.provider.DocumentsContract.Document.COLUMN_SIZE


class PaymentHistoryActivity : BaseActivity() {

    val mColumnHeader: List<String> = object : ArrayList<String>() {
        init {
            add("No")
            add("date")
            add("year")
            add("man")
        }
    }


    val mRowHeader: List<String> = object : ArrayList<String>() {
        init {
            add("mRowHeader")
            add("mRowHeader")
            add("v")
            add("mRowHeader")
        }
    }
    val mCells: List<List<String>> = object : ArrayList<List<String>>() {
        init {
            add(mRowHeader)
            add(mColumnHeader)
            add(mColumnHeader)
            add(mColumnHeader)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_history)
        //setupToolbar()
        initView()
    }

    private fun initView() {
        val myTableAdapter = MyTableViewAdapter(PaymentHistoryActivity@ this)
        tbl_history.adapter = myTableAdapter
        myTableAdapter.setAllItems(getRandomColumnHeaderList(), getSimpleRowHeaderList(), getCellListForSortingTest())
    }

    private fun setupToolbar() {
        txt_title_toolbar.text = getString(R.string.payment_history)
        setSupportActionBar(findViewById(R.id.include))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Constant size for dummy data sets
    private val COLUMN_SIZE = 500
    private val ROW_SIZE = 500

    val MOOD_COLUMN_INDEX = 3
    val GENDER_COLUMN_INDEX = 4

    val SAD = 1
    val HAPPY = 2
    val BOY = 1
    val GIRL = 2

    private fun getRandomColumnHeaderList(): List<String> {
        val mList = ArrayList<String>()

        for (i in 0..10){
            Log.d("TAG_JUSTIN", "justin")

            var title = "column $i"
            val nRandom = Random().nextInt()
            if (nRandom % 4 == 0 || nRandom % 3 == 0 || nRandom == i) {
                title = "large column $i"
            }
            mList.add(title)
        }

        return mList
    }

    private fun getSimpleRowHeaderList(): List<String> {
        val list = ArrayList<String>()
        for (i in 0 until ROW_SIZE) {
            val header ="$i"
            list.add(header)
        }

        return list
    }


    private fun getCellListForSortingTest(): List<List<String>> {
        val list = ArrayList<List<String>>()
        for (i in 0 until ROW_SIZE) {
            val cellList = ArrayList<String>()
            for (j in 0 until COLUMN_SIZE) {
                var text = "cell $j $i"

               /* val random = Random().nextInt()
                if (j == 0) {
                    text = i
                } else if (j == 1) {
                    text = random
                } else if (j == MOOD_COLUMN_INDEX) {
                    text = if (random % 2 == 0) HAPPY else SAD
                } else if (j == GENDER_COLUMN_INDEX) {
                    text = if (random % 2 == 0) BOY else GIRL
                }*/

                // Create dummy id.
                val id = "$j-$i"

                val cell: String
               /* if (j == 3) {
                    cell = Cell(id, text)
                } else if (j == 4) {
                    // NOTE female and male keywords for filter will have conflict since "female"
                    // contains "male"
                    cell = Cell(id, text)
                } else {
                    cell = Cell(id, text)
                }*/
                cellList.add(text)
            }
            list.add(cellList)
        }

        return list
    }


}
