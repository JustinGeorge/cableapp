package com.justin.cablenetwork.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.menu.MenuBuilder
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.justin.cablenetwork.R
import com.justin.cablenetwork.adapters.ChannelFragmentsAdapter
import com.justin.cablenetwork.callbacks.SubscribedCallback
import com.justin.cablenetwork.fragments.AlaCarteChannelListFragment
import com.justin.cablenetwork.fragments.AllChannelListFragment
import com.justin.cablenetwork.fragments.SubscribedChannelsFragment
import com.justin.cablenetwork.models.Channel
import com.justin.cablenetwork.models.ItemUserListModel
import com.justin.cablenetwork.models.SubscribedChannel
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_channel_list.*
import kotlinx.android.synthetic.main.layout_toolbar.my_toolbar
import kotlinx.android.synthetic.main.layout_toolbar.txt_title_toolbar
import kotlinx.android.synthetic.main.layout_toolbar_cart.*
import java.util.*


class ChannelListActivity : BaseActivity(), SubscribedCallback, View.OnClickListener {

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fl_cart -> {
                //Toast.makeText(this@ChannelListActivity,"clicked ",Toast.LENGTH_SHORT).show()
                printCart()
                gotoPaymentActivity()
            }
        }
    }

    private fun gotoPaymentActivity() {
        val paymentActivityIntent = Intent(this@ChannelListActivity, PaymentActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("param_user_info", itemUserListModel)
        paymentActivityIntent.putExtras(bundle)
        startActivity(paymentActivityIntent)
    }

    private fun printCart() {
        Log.d("TAG_JUSTIN", "=====================================Cart============================================")

        for (item in mCart) {
            Log.d("TAG_JUSTIN", "" + item.toString())
        }

        Log.d("TAG_JUSTIN", "=================================unSubscribed========================================")

        for (id in mUnSubscribedList) {
            Log.d("TAG_JUSTIN", "" + id.toString())
        }
    }


    private lateinit var mChannelFragmentsAdapter: ChannelFragmentsAdapter
    private lateinit var itemUserListModel: ItemUserListModel
    private lateinit var mToken: String

    private val mCart = LinkedList<Channel>()
    private val mUnSubscribedList = LinkedList<Long>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_channel_list)
        getUserDataBundle()
        initView()
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_all_channels, menu)
        // Get the SearchView and set the searchable configuration
        if (menu is MenuBuilder) {
            menu.setOptionalIconsVisible(true)
        }
        return super.onCreateOptionsMenu(menu)
    }


    override fun setPrepareCart(mSubscribedList: List<SubscribedChannel>) {

    }

    override fun onSubscribed(channel: Channel?) {
        //Toast.makeText(this@ChannelListActivity,"subscribed ",Toast.LENGTH_SHORT).show()
        if (channel != null) {

            if (mUnSubscribedList.contains(channel.id)) {
                Log.d("TAG_JUSTIN", "unsubscribe " + channel.id)
                mUnSubscribedList.remove(channel.id)
            } else {
                mCart.add(channel)
                updateCart()
            }
        }
    }

    private fun updateCart() {
        //  txt_cart_count.text = mCart.size.toString()
    }

    override fun onUnSubscribed(id: Long?) {
        //Toast.makeText(this@ChannelListActivity,"unSubscribed ",Toast.LENGTH_SHORT).show()
        if (id != null) {
            val iterator = mCart.iterator()
            while (iterator.hasNext()) {
                if (iterator.next().id == id) {
                    iterator.remove()
                    updateCart()
                    return
                }
            }
            mUnSubscribedList.add(id)
        }
    }


    private fun getUserDataBundle() {
        itemUserListModel = intent.extras.get("param_Item_user") as ItemUserListModel
        mToken = SharedPreferenceManager.getInstance(this@ChannelListActivity)
            .getString(PreferenceUtils.LoginConstants.token)
    }

    private fun initView() {
        txt_title_toolbar.text = "${itemUserListModel.customerName}'s Channels"
        fl_cart.setOnClickListener(this@ChannelListActivity)

        setSupportActionBar(my_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        mChannelFragmentsAdapter = ChannelFragmentsAdapter(supportFragmentManager)

        mChannelFragmentsAdapter.addFragment(
            SubscribedChannelsFragment(itemUserListModel, mToken),
            "Subscribed Channels"
        )
        mChannelFragmentsAdapter.addFragment(
            AllChannelListFragment(itemUserListModel, mToken),
            "Bouquet"
        )

        mChannelFragmentsAdapter.addFragment(
            AlaCarteChannelListFragment(itemUserListModel, mToken),
            "Ala Carte"
        )


        pager.offscreenPageLimit = 0
        pager.adapter = mChannelFragmentsAdapter
        tab_layout.setupWithViewPager(pager)


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {

            android.R.id.home -> finish()

            R.id.action_payment_history -> {
                //Toast.makeText(this@ChannelListActivity,"Payment history", Toast.LENGTH_SHORT).show()
                gotoPaymentHistoryActivity()
            }

            R.id.action_Payment -> {
               // Toast.makeText(this@ChannelListActivity,"Payment ", Toast.LENGTH_SHORT).show()
                gotoPaymentActivity()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun gotoPaymentHistoryActivity() {
        val paymentActivityIntent = Intent(this@ChannelListActivity, UserPaymentHistoryActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("param_user_info", itemUserListModel)
        bundle.putSerializable("param_user_info", itemUserListModel)
        paymentActivityIntent.putExtras(bundle)
        startActivity(paymentActivityIntent)
    }


}

