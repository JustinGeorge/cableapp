package com.justin.cablenetwork.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.justin.cablenetwork.adapters.UserListAdapter
import com.justin.cablenetwork.models.ItemUserListModel
import com.justin.cablenetwork.models.UserListModel
import com.justin.cablenetwork.network.ApiClient
import com.justin.cablenetwork.network.ApiInterface
import com.justin.cablenetwork.preferences.PreferenceUtils
import com.justin.cablenetwork.preferences.SharedPreferenceManager
import com.justin.cablenetwork.R
import com.justin.cablenetwork.utils.ItemOffsetDecoration
import com.justin.cablenetwork.utils.LogWriter
import com.justin.cablenetwork.utils.NetworkConnectionUtils
import kotlinx.android.synthetic.main.activity_home_acivity.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class HomeActivity : BaseActivity(), View.OnClickListener, SearchView.OnQueryTextListener, View.OnFocusChangeListener,
    SearchView.OnCloseListener, UserListAdapter.UserItemClickListener {

    override fun onUserItemClick(itemUserListModel: ItemUserListModel) {
        val intent = Intent(this@HomeActivity, ChannelListActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("param_Item_user",itemUserListModel)
        intent.putExtras(bundle)
        startActivity(intent)
        //finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )

    }


    private val ROWS_PER_PAGE = 20
    private var PAGE_NUMBER = 0

    private lateinit var layoutManager: LinearLayoutManager
    private var mUserList = ArrayList<ItemUserListModel>()
    private lateinit var mAdapter: UserListAdapter
    private var isLoading = false
    private var isSearchEnabled = false
    private var searchQuery: String? = null
    private var mSearView: SearchView? = null
    private var mTimer: Timer? = null

    override fun onFocusChange(v: View?, hasFocus: Boolean) {

    }

    override fun onClose(): Boolean {
        Log.d("TAG_JUSTIN_FOCUS", "  onClose  :  onFocuChange  : " + isSearchEnabled)
        if (isSearchEnabled) {
            this.isSearchEnabled = false
            PAGE_NUMBER = 0
            mUserList?.clear()
            mAdapter?.notifyDataSetChanged()
            getUserList()
        }
        Log.d("TAG_JUSTIN_FOCUS", "onFocuChange  : " + isSearchEnabled)
        return false
    }


    override fun onQueryTextSubmit(query: String?): Boolean {
        isSearchEnabled = true
        Log.d("TAG_JUSTIN_FOCUS", "onQueryTextSubmit  :  onFocuChage  : " + isSearchEnabled)

        searchQuery = query
        // Toast.makeText(this@HomeActivity, "QueryTextSubmit : " + searchQuery, Toast.LENGTH_SHORT).show()
        PAGE_NUMBER = 0
        mUserList?.clear()
        mAdapter?.notifyDataSetChanged()
        getSearchResult()
        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        mTimer?.cancel()
        mTimer?.purge()

        mTimer = Timer()
        mTimer?.schedule(object : TimerTask() {
            override fun run() {
                if (query != null && query.isNotEmpty()) {
                    runOnUiThread {
                        Log.d("TAG_QUERY", "query string : $query")
                        isSearchEnabled = true
                        Log.d("TAG_JUSTIN_FOCUS", "onQueryTextSubmit  :  onFocuChage  : " + isSearchEnabled)
                        searchQuery = query
                        PAGE_NUMBER = 0
                        mUserList?.clear()
                        mAdapter?.notifyDataSetChanged()
                        getSearchResult()
                    }
                }
            }
        }, 1000)
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab -> {
                /*  startActivity(Intent(this@HomeActivity, UserSearchActivity::class.java))
                  overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)*/
                //   onSearchRequested()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_acivity)
        initView()
        setPaymentHistory()
        getUserList()
    }

    private fun getUserList() {

        if (!NetworkConnectionUtils.getConnectionStatus(this@HomeActivity)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }

        if (PAGE_NUMBER == 0) {
            showSimpleProgressAlert(this)
        }
        isLoading = true
        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getUserList(
            SharedPreferenceManager.getInstance(this@HomeActivity).getString(PreferenceUtils.LoginConstants.token),
            PAGE_NUMBER,
            ROWS_PER_PAGE
        )

        call?.enqueue(object : Callback<UserListModel> {

            override fun onFailure(call: Call<UserListModel>, t: Throwable) {
                hideSimpleProgressAlert()
                isLoading = false
                LogWriter.getInstance().Log("Home userlist Failed response : " + t.message + "\n" + t.localizedMessage)

            }

            override fun onResponse(call: Call<UserListModel>, response: Response<UserListModel>) {
                hideSimpleProgressAlert()
                LogWriter.getInstance().Log("Home userlit Success response : " + response.body()!!.toString())

                isLoading = false
                if (response.body()?.success!!) {
                    getUserListModels(response.body()!!)
                }
            }
        })
    }


    private fun getSearchResult() {

        if (!NetworkConnectionUtils.getConnectionStatus(this@HomeActivity)) {
            Snackbar.make(
                coordinatorLayout_layout_home,
                resources.getString(R.string.no_connection),
                Snackbar.LENGTH_SHORT
            )
                .show()
            return
        }
        if (searchQuery.isNullOrEmpty()) {
            Snackbar.make(constraint_layout_login, resources.getString(R.string.no_search_query), Snackbar.LENGTH_SHORT)
                .show()
            return
        }
        if (PAGE_NUMBER == 0) {
            showSimpleProgressAlert(this)
        }
        isLoading = true
        val apiService = ApiClient.get()?.create(ApiInterface::class.java)
        val call = apiService?.getSearchResult(
            SharedPreferenceManager.getInstance(this@HomeActivity).getString(PreferenceUtils.LoginConstants.token),
            PAGE_NUMBER,
            ROWS_PER_PAGE,
            searchQuery
        )

        call?.enqueue(object : Callback<UserListModel> {
            override fun onFailure(call: Call<UserListModel>, t: Throwable) {

                hideSimpleProgressAlert()
                isLoading = false

                LogWriter.getInstance().Log("Search Failed response : " + t.message + "\n" + t.localizedMessage)

            }

            override fun onResponse(call: Call<UserListModel>, response: Response<UserListModel>) {
                hideSimpleProgressAlert()
                isLoading = false
                if (response.body()?.success!!) {
                    getUserListModels(response.body()!!)
                }
                LogWriter.getInstance().Log("Search Success response : " + response.body()!!.toString())

            }
        })
    }

    private fun getUserListModels(body: UserListModel) {
        if (body.data!!.isNotEmpty()) {
            if (PAGE_NUMBER != 0) {
                Snackbar.make(recy_payment_history, "List updated...", Snackbar.LENGTH_SHORT).show()
            }
            mUserList.addAll(body.data!!)
            PAGE_NUMBER++
            txt_no_data.visibility = View.GONE
            mAdapter.notifyDataSetChanged()
        } else {
            if (PAGE_NUMBER == 0) {
                txt_no_data.visibility = View.VISIBLE
            }
        }
    }


    private fun setPaymentHistory() {
        layoutManager = LinearLayoutManager(this)
        recy_payment_history.layoutManager = layoutManager
        recy_payment_history.addItemDecoration(ItemOffsetDecoration(this, R.dimen.item_offset))
        mAdapter = UserListAdapter(this, mUserList)
        recy_payment_history.adapter = mAdapter
        recy_payment_history.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.getChildCount()
                val totalItemCount = layoutManager.getItemCount()
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if (!isLoading) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE
                    ) {
                        Log.d("TAG_JUSTIN_FOCUS", "setPaymentHistory :  onFocuChange  : " + isSearchEnabled)
                        if (isSearchEnabled) {
                            getSearchResult()
                        } else {
                            getUserList()
                        }
                        Snackbar.make(recy_payment_history, "Loading.....", Snackbar.LENGTH_SHORT).show()

                    }
                }
            }
        })
    }

    private fun initView() {
        mUserList.clear()
        mTimer = Timer()
        txt_title_toolbar.text = "User List"
        setSupportActionBar(my_toolbar)
        fab.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_home, menu)
        // Get the SearchView and set the searchable configuration
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        mSearView = menu?.findItem(R.id.action_search)?.actionView as SearchView

        (mSearView).apply {
            // Assumes current activity is the searchable activity
            this!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextFocusChangeListener(this@HomeActivity)
            // setIconifiedByDefault(false) // Do not iconify the widget; expand it by default
            setOnQueryTextListener(this@HomeActivity)

            setOnCloseListener(this@HomeActivity)

        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.action_change_password -> {
                startActivity(Intent(this@HomeActivity, PasswordActivity::class.java))
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            }

            R.id.action_log_out -> {
                doLogOut()
            }

            R.id.action_exit -> {
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun doLogOut() {
        SharedPreferenceManager.getInstance(this@HomeActivity).doLogout()
        val intent = Intent(this@HomeActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        startActivity(intent)
        finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )
    }
}
