package com.justin.cablenetwork.callbacks

import com.justin.cablenetwork.models.Channel
import com.justin.cablenetwork.models.SubscribedChannel

interface SubscribedCallback {
    fun onSubscribed(channel : Channel?)
    fun onUnSubscribed(id:Long?)
    fun setPrepareCart(mSubscribedList: List<SubscribedChannel>)
}