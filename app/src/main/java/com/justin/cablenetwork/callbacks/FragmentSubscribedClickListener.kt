package com.justin.cablenetwork.callbacks

import com.justin.cablenetwork.models.Channel

interface FragmentSubscribedClickListener {
    fun onSubscribed(channel : Channel)
    fun onUnSubscribed(channel : Channel)
    fun onUnSubscribed(id: Long?)

}