package com.justin.cablenetwork.application

import android.app.Application
import com.justin.cablenetwork.preferences.SharedPreferenceManager


class CableApplication : Application() {

 private lateinit var  mSharePreferenceManager: SharedPreferenceManager

    override fun onCreate() {
        super.onCreate()
        mSharePreferenceManager = SharedPreferenceManager.getInstance(applicationContext)
    }
}