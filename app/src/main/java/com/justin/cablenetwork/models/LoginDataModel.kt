package com.justin.cablenetwork.models

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class LoginDataModel : Serializable {
    @SerializedName("unique_id")
    @Expose
     var uniqueId: Int? = null
    @SerializedName("First_name")
    @Expose
     var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    public var lastName: String? = null
    @SerializedName("address_1")
    @Expose
    public var address1: String? = null
    @SerializedName("address_2")
    @Expose
    public var address2: Any? = null
    @SerializedName("address_3")
    @Expose
    public var address3: Any? = null
    @SerializedName("city")
    @Expose
    public var city: String? = null
    @SerializedName("pincode")
    @Expose
    public var pincode: String? = null
    @SerializedName("email_id")
    @Expose
    public var emailId: String? = null
    @SerializedName("phone")
    @Expose
    public var phone: Any? = null
    @SerializedName("mobile")
    @Expose
    public var mobile: String? = null
    @SerializedName("user_type")
    @Expose
    public var userType: String? = null
    @SerializedName("user_role")
    @Expose
    public var userRole: String? = null
    @SerializedName("device_id")
    @Expose
    public var deviceId: String? = null
    @SerializedName("created_by")
    @Expose
    public var createdBy: String? = null
    @SerializedName("createdtimestamp")
    @Expose
    public var createdtimestamp: String? = null
    @SerializedName("updated_by")
    @Expose
    public var updatedBy: Any? = null
    @SerializedName("updatedtimestamp")
    @Expose
    public var updatedtimestamp: Any? = null
    @SerializedName("first_login_indicator")
    @Expose
    public var firstLoginIndicator: Boolean? = null

    override fun toString(): String {
        return "LoginDataModel(uniqueId=$uniqueId, firstName=$firstName, lastName=$lastName, address1=$address1, address2=$address2, address3=$address3, city=$city, pincode=$pincode, emailId=$emailId, phone=$phone, mobile=$mobile, userType=$userType, userRole=$userRole, deviceId=$deviceId, createdBy=$createdBy, createdtimestamp=$createdtimestamp, updatedBy=$updatedBy, updatedtimestamp=$updatedtimestamp, firstLoginIndicator=$firstLoginIndicator)"
    }


}