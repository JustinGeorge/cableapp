package com.justin.cablenetwork.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PaymentHistory {
    constructor(year: Int?, month: Int?, monthName: String?, sumAmount: Int?) {
        this.year = year
        this.month = month
        this.monthName = monthName
        this.sumAmount = sumAmount
    }

    @SerializedName("Year")
    @Expose
     var year: Int? = null
    @SerializedName("Month")
    @Expose
     var month: Int? = null
    @SerializedName("Month Name")
    @Expose
     var monthName: String? = null
    @SerializedName("Sum Amount")
    @Expose
     var sumAmount: Int? = null
}
