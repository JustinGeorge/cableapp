package com.justin.cablenetwork.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AllChannelsResponse : Serializable {

    @SerializedName("data")
    var data: List<List<ChannelInfo>>? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("success")
    var success: Boolean? = null

}
