package com.justin.cablenetwork.models

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ChangePasswordDataModel : Serializable{
    @SerializedName("umessage")
    @Expose
    var umessage: String? = null

    override fun toString(): String {
        return "ChangePasswordDataModel(umessage=$umessage)"
    }

}
