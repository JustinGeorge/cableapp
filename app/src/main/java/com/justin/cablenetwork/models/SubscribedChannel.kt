package com.justin.cablenetwork.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SubscribedChannel : Serializable  {

    @SerializedName("CustomerId")
    var customerId: String? = null
    @SerializedName("EndDate")
    var endDate: String? = null
    @SerializedName("Id")
    var id: Long? = null
    @SerializedName("IsActive")
    var isActive: Boolean? = null
    @SerializedName("Name")
    var name: String? = null
    @SerializedName("Price")
    var price: String? = null
    @SerializedName("StartDate")
    var startDate: String? = null
    @SerializedName("PackageId")
    var packageId: Long? = null

    var isSelected:Boolean = true

    override fun toString(): String {
        return "SubscribedChannel(customerId=$customerId, endDate=$endDate, id=$id, isActive=$isActive, name=$name, price=$price, startDate=$startDate, packageId=$packageId, isSelected=$isSelected)"
    }


}
