package com.justin.cablenetwork.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PrintBillResponse {
    @SerializedName("success")
    @Expose
     var success: Boolean? = null
    @SerializedName("message")
    @Expose
     var message: String? = null
    @SerializedName("BranchMaster")
    @Expose
     var branchMaster: List<BranchMaster>? = null
    @SerializedName("customer_master")
    @Expose
     var customerMaster: List<CustomerMaster>? = null
    @SerializedName("packageMaster")
    @Expose
     var packageMaster: List<PackageMaster>? = null
    @SerializedName("BillDetails")
    @Expose
     var billDetails: List<BillDetail>? = null

}