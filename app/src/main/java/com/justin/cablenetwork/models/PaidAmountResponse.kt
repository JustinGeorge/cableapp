package com.justin.cablenetwork.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PaidAmountResponse : Serializable {

    @SerializedName("message")
    var message: String? = null
    @SerializedName("success")
    var success: Boolean? = null

}
