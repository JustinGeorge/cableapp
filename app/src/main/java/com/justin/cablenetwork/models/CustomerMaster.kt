package com.justin.cablenetwork.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CustomerMaster {
    @SerializedName("customer_name")
    @Expose
     val customerName: String? = null
    @SerializedName("last_name")
    @Expose
     val lastName: String? = null
    @SerializedName("area_1")
    @Expose
     val area1: String? = null
    @SerializedName("area_2")
    @Expose
     val area2: String? = null
    @SerializedName("location")
    @Expose
     val location: String? = null
    @SerializedName("pincode")
    @Expose
     val pincode: String? = null
    @SerializedName("country")
    @Expose
     val country: String? = null
    @SerializedName("state")
    @Expose
     val state: String? = null
    @SerializedName("phone")
    @Expose
     val phone: String? = null
    @SerializedName("mobile")
    @Expose
     val mobile: String? = null
    @SerializedName("email_id")
    @Expose
     val emailId: String? = null
    @SerializedName("stb_number")
    @Expose
     val stbNumber: String? = null
    @SerializedName("vc_card_no")
    @Expose
     val vcCardNo: String? = null
    @SerializedName("unique_customer_number")
    @Expose
     val uniqueCustomerNumber: String? = null
}
