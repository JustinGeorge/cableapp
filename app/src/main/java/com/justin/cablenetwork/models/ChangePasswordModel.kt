package com.justin.cablenetwork.models

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ChangePasswordModel : Serializable {
    @SerializedName("success")
    @Expose
    var success: Boolean? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: List<ChangePasswordDataModel>? = null

    override fun toString(): String {
        return "ChangePasswordModel(success=$success, message=$message, data=$data)"
    }


}

