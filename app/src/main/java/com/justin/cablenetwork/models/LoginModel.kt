package com.justin.cablenetwork.models

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class LoginModel :  Serializable {
    @SerializedName("success")
    @Expose
    var success: Boolean? = null
    @SerializedName("token")
    @Expose
     var token: String? = null
    @SerializedName("data")
    @Expose
     var data: List<LoginDataModel>? = null

    override fun toString(): String {
        return "LoginModel(success=$success, token=$token, data=$data)"
    }



}