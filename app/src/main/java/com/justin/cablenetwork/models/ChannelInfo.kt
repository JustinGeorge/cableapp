package com.justin.cablenetwork.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ChannelInfo : Serializable {

    @SerializedName("Genre")
    var genre: String? = null
    @SerializedName("Id")
    var id: Long? = null
    @SerializedName("Language")
    var language: String? = null
    @SerializedName("Name")
    var name: String? = null
    @SerializedName("Price")
    var price: String? = null

    var isSelected:Boolean = false

}
