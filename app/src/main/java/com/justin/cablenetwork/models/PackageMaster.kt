package com.justin.cablenetwork.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PackageMaster {
    @SerializedName("Id")
    @Expose
     val id: String? = null
    @SerializedName("CustomerId")
    @Expose
     val customerId: String? = null
    @SerializedName("IsActive")
    @Expose
     val isActive: Boolean? = null
    @SerializedName("StartDate")
    @Expose
     val startDate: String? = null
    @SerializedName("EndDate")
    @Expose
     val endDate: String? = null
    @SerializedName("Name")
    @Expose
     val name: String? = null
    @SerializedName("Price")
    @Expose
     val price: String? = null
    @SerializedName("PackageId")
    @Expose
     val packageId: String? = null

    override fun toString(): String {
        return "PackageMaster(id=$id, customerId=$customerId, isActive=$isActive, startDate=$startDate, endDate=$endDate, name=$name, price=$price, packageId=$packageId)"
    }


}
