package com.justin.cablenetwork.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class BillDetail {
    @SerializedName("TotalPrice")
    @Expose
     val totalPrice: String? = null
    @SerializedName("CGST")
    @Expose
     val cGST: String? = null
    @SerializedName("SGST")
    @Expose
     val sGST: String? = null
    @SerializedName("GrandTotal")
    @Expose
     val grandTotal: String? = null
    @SerializedName("Lastbalance")
    @Expose
     val lastbalance: String? = null
    @SerializedName("Currentbalance")
    @Expose
     val currentbalance: String? = null
    @SerializedName("receiptNo")
    @Expose
     val receiptNo: String? = null
}
