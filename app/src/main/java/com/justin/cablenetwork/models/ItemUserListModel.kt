package com.justin.cablenetwork.models

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ItemUserListModel : Serializable {

    @SerializedName("unique_id")
    @Expose
    var uniqueId: Int? = null
    @SerializedName("customer_name")
    @Expose
    var customerName: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: Any? = null
    @SerializedName("area_1")
    @Expose
    var area1: String? = null
    @SerializedName("area_2")
    @Expose
    var area2: Any? = null
    @SerializedName("location")
    @Expose
    var location: String? = null
    @SerializedName("pincode")
    @Expose
    var pincode: String? = null
    @SerializedName("country")
    @Expose
    var country: Any? = null
    @SerializedName("state")
    @Expose
    var state: Any? = null
    @SerializedName("phone")
    @Expose
    var phone: Any? = null
    @SerializedName("mobile")
    @Expose
    var mobile: String? = null
    @SerializedName("email_id")
    @Expose
    var emailId: Any? = null
    @SerializedName("installation_charges")
    @Expose
    var installationCharges: Any? = null
    @SerializedName("group_details")
    @Expose
    var groupDetails: String? = null
    @SerializedName("unique_customer_number")
    @Expose
    var uniqueCustomerNumber: String? = null
    @SerializedName("package_name")
    @Expose
    var packageName: String? = null
    @SerializedName("package")
    @Expose
    var _package: Int? = null
    @SerializedName("outstanding")
    @Expose
    var outstanding: String? = null
    @SerializedName("stb_number")
    @Expose
    var stbNumber: String? = null
    @SerializedName("vc_card_no")
    @Expose
    var vcCardNo: Int? = null
    @SerializedName("mso_name")
    @Expose
    var msoName: String? = null
    @SerializedName("mso_id")
    @Expose
    var msoId: Int? = null
    @SerializedName("connection_date")
    @Expose
    var connectionDate: Any? = null
    @SerializedName("dateofbirth")
    @Expose
    var dateofbirth: Any? = null
    @SerializedName("anniversary_date")
    @Expose
    var anniversaryDate: Any? = null
    @SerializedName("remarks")
    @Expose
    var remarks: Any? = null
    @SerializedName("subscription_startdate")
    @Expose
    var subscriptionStartdate: Any? = null
    @SerializedName("subscription_enddate")
    @Expose
    var subscriptionEnddate: Any? = null
    @SerializedName("subscription_re_join_date")
    @Expose
    var subscriptionReJoinDate: Any? = null
    @SerializedName("created_by")
    @Expose
    var createdBy: String? = null
    @SerializedName("createdtimestamp")
    @Expose
    var createdtimestamp: String? = null
    @SerializedName("updated_by")
    @Expose
    var updatedBy: Any? = null
    @SerializedName("updatedtimestamp")
    @Expose
    var updatedtimestamp: Any? = null

    override fun toString(): String {
        return "ItemUserListModel(uniqueId=$uniqueId, customerName=$customerName, lastName=$lastName, area1=$area1, area2=$area2, location=$location, pincode=$pincode, country=$country, state=$state, phone=$phone, mobile=$mobile, emailId=$emailId, installationCharges=$installationCharges, groupDetails=$groupDetails, uniqueCustomerNumber=$uniqueCustomerNumber, packageName=$packageName, _package=$_package, outstanding=$outstanding, stbNumber=$stbNumber, vcCardNo=$vcCardNo, msoName=$msoName, msoId=$msoId, connectionDate=$connectionDate, dateofbirth=$dateofbirth, anniversaryDate=$anniversaryDate, remarks=$remarks, subscriptionStartdate=$subscriptionStartdate, subscriptionEnddate=$subscriptionEnddate, subscriptionReJoinDate=$subscriptionReJoinDate, createdBy=$createdBy, createdtimestamp=$createdtimestamp, updatedBy=$updatedBy, updatedtimestamp=$updatedtimestamp)"
    }


}
