package com.justin.cablenetwork.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class BranchMaster {
    @SerializedName("Id")
    @Expose
     val id: Int? = null
    @SerializedName("BranchName")
    @Expose
     val branchName: String? = null
    @SerializedName("phonenumber")
    @Expose
     val phonenumber: String? = null
    @SerializedName("address")
    @Expose
     val address: String? = null
    @SerializedName("city")
    @Expose
     val city: String? = null
    @SerializedName("datestamp")
    @Expose
     val datestamp: String? = null
    @SerializedName("pincode")
    @Expose
     val pincode: Int? = null
}
