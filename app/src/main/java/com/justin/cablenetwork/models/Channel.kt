package com.justin.cablenetwork.models

data class Channel(val id: Long?, val name: String?, val price: String?)

