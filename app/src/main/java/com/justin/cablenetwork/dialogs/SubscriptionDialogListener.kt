package com.justin.cablenetwork.dialogs

import com.justin.cablenetwork.models.Channel
import java.util.*

interface SubscriptionDialogListener {
    fun onConfirm()
}