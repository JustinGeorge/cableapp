package com.justin.cablenetwork.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import com.justin.cablenetwork.R
import com.justin.cablenetwork.fragments.AllChannelListFragment
import com.justin.cablenetwork.fragments.AllChannelsBaseFragment
import com.justin.cablenetwork.models.Channel
import com.justin.cablenetwork.utils.ItemOffsetDecoration
import kotlinx.android.synthetic.main.dialog_channel_list.*
import java.util.*

class ChannelListDialog(
    context: Context,
    title: String,
    channelList: LinkedList<Channel>,
    allChannelListFragment: AllChannelsBaseFragment
) : Dialog(context),
    View.OnClickListener, SubscriptionDialogRecyclerListener {


    override fun onCheckChanged(channel: Channel) {
        val iterator = mChannelList.iterator()
        while (iterator.hasNext()) {
            if (iterator.next().id == channel.id) {
                iterator.remove()
                break
            }
        }
        mChannelsAdapter.notifyDataSetChanged()
    }

    override fun onPositiveClick() {
        hide()
        mSubscriptionDialogListener.onConfirm()
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.txt_confirm -> {
                mSubscriptionDialogListener.onConfirm()
                hide()
            }
            R.id.txt_cancel -> {
                hide()
            }
        }
    }

    private val mContext = context
    private val mTitle = title
    private val mChannelList = channelList
    private val mSubscriptionDialogListener = allChannelListFragment as SubscriptionDialogListener
    private lateinit var mChannelsAdapter: SubscriptionDialogChannelListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_channel_list)
        initView()
    }

    private fun initView() {
        txt_title.text = mTitle
        txt_cancel.setOnClickListener(this)
        txt_confirm.setOnClickListener(this)
        updateRecyclerView()
        updatePrice()
    }

    private fun updatePrice() {
        var additionalPrice = 0.0
        for (channel in mChannelList){
            if (channel.price !=null)
            additionalPrice += channel.price.toFloat()
        }
        txt_additional_price.text = "Amount to be paid : ${String.format("%.2f", additionalPrice).toDouble()}"
    }

    private fun updateRecyclerView() {
        mChannelsAdapter = SubscriptionDialogChannelListAdapter(this, mChannelList)
        recy_dialog_list_channels.layoutManager = LinearLayoutManager(mContext)
        recy_dialog_list_channels.addItemDecoration(ItemOffsetDecoration(mContext, R.dimen.item_offset))
        recy_dialog_list_channels.adapter = mChannelsAdapter

    }

    fun setPositiveButtonTitle(title: String) {
        txt_confirm.text = title
    }


}