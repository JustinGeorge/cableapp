package com.justin.cablenetwork.dialogs

interface AddPrinterDialogListener {
    fun onAddClicked(name : String)
}