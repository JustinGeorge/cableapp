package com.justin.cablenetwork.dialogs

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.justin.cablenetwork.R
import com.justin.cablenetwork.models.ItemUserListModel
import com.justin.cablenetwork.models.Channel
import kotlinx.android.synthetic.main.item_channel_activation.view.*
import kotlinx.android.synthetic.main.item_channel_activation_text.view.*
import java.util.*

class SubscriptionDialogChannelListAdapter(
    context: ChannelListDialog,
    mChannelList: LinkedList<Channel>
) : RecyclerView.Adapter<SubscriptionDialogChannelListAdapter.PaymentItemViewHolder>() {

    private val mContext : ChannelListDialog = context
    private val mChannelList :  LinkedList<Channel> = mChannelList
    private var mSubscriptionDialogActionListener : SubscriptionDialogRecyclerListener

    init {
        this.mSubscriptionDialogActionListener = mContext as SubscriptionDialogRecyclerListener
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PaymentItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_channel_activation_text, parent, false)
        return PaymentItemViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mChannelList.size
    }

    override fun onBindViewHolder(viewHolder: PaymentItemViewHolder, position: Int) {
        viewHolder.bind(mChannelList.get(position), mSubscriptionDialogActionListener)
    }


    class PaymentItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(
            data: Channel,
            mUserItemClickListener: SubscriptionDialogRecyclerListener
        ) {
            mCheckBox.text = data.name
            /*mCheckBox.setOnClickListener {

                mUserItemClickListener.onCheckChanged(data)

            }*/
        }

        private val mCheckBox = itemView.txt_channel

    }
}