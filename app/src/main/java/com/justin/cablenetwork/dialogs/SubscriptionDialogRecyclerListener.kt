package com.justin.cablenetwork.dialogs

import com.justin.cablenetwork.models.Channel

interface SubscriptionDialogRecyclerListener {
    fun onCheckChanged(channel: Channel)
    fun onPositiveClick()
}