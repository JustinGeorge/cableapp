package com.justin.cablenetwork.dialogs

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import com.justin.cablenetwork.R
import kotlinx.android.synthetic.main.layout_dialog_add_printer.view.*

class Dialogs {

    private lateinit var mAddPrinterDialog: AlertDialog
    private val TAG = "TAG_PRINTER"

    fun showAddPrinterAlertDialog(context: Context){
        val mAddPrinterDialogListener = context as AddPrinterDialogListener
        val mDialogView  = LayoutInflater.from(context).inflate(R.layout.layout_dialog_add_printer,null)
        mAddPrinterDialog = AlertDialog.Builder(context,R.style.CustomProgressBarTheme).create()
        mAddPrinterDialog.setTitle("Add printer")
        mAddPrinterDialog.setCancelable(false)
        mAddPrinterDialog.setMessage("Enter printer name as showing in the settings screen")
        mAddPrinterDialog.setView(mDialogView)


        mAddPrinterDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Add"
        ) { dialog, which ->
            Toast.makeText(context,"Positive button clicked in dialog!",Toast.LENGTH_SHORT).show()
            Log.d(TAG,"Positive button clicked in dialog!")
            mAddPrinterDialogListener.onAddClicked( mDialogView.edtxt_printer_name.text.toString())
        }

        mAddPrinterDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, which ->

        }

        mAddPrinterDialog.show()
    }

}