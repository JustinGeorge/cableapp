package com.justin.cablenetwork.bluetoothprinter;

import android.app.Activity;
import android.content.Context;

import java.text.SimpleDateFormat;


public class AppGlobals {
    public static String EmpId = "";

    public static int IsAdminUser;

    public static String Token = "";

    public static int SelectedCustomerId = -1;

    private static SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");

    private static Context context;

    public static void setContext(Context ctx) {
        context = ctx;
    }

    public static Context getContext() {
        return context;
    }

    public static SimpleDateFormat AppDateFormat() {
        return curFormater;
    }


    public static Activity MainActivity;

    public static String PrinterName = "";//"BTP-B00955";

    public static String PrinterMac = "";

    public static String ServerUrl = "";

    public static String CompanyName = "SITI CHANNEL";

    public static String TagLine = "Challakere";

    public static boolean IsBluetoothConnected;

    public static PrinterTestDialogFragment printerTestDialogFragment = new PrinterTestDialogFragment();

    public static String MacId = "";

    public static int PrinterCharWidth = 38;

}
