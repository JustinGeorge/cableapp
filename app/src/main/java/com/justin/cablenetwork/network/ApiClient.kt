package com.justin.cablenetwork.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.X509Certificate
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


object ApiClient {

        var retrofit: Retrofit? = null
        val BASE_URL = "http://ec2-3-16-113-238.us-east-2.compute.amazonaws.com:3008/"
        fun get(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }

        private fun getOkHttpClient(): OkHttpClient {

            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate>? = null
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) = Unit
                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) = Unit
            })


            var okHttpClientBuilder = OkHttpClient.Builder()

            val socketFactory = SSLContext.getInstance("SSL").apply {
                init(null, trustAllCerts, java.security.SecureRandom())
            }.socketFactory

            val hostnameVerifier = HostnameVerifier { _, _ -> true }

            okHttpClientBuilder.sslSocketFactory(socketFactory,object : X509TrustManager{
                override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            })
            okHttpClientBuilder.hostnameVerifier(hostnameVerifier)

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY


            okHttpClientBuilder
                .addInterceptor(interceptor)

            return okHttpClientBuilder.build()
        }

}