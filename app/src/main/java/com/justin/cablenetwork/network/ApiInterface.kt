package com.justin.cablenetwork.network


import com.justin.cablenetwork.models.*
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface ApiInterface {

    @POST("User/login")
    @FormUrlEncoded
    fun getLoginRsponse(
        @Field("mobilenumber") mobileNumber: String,
        @Field("password") password: String
    ): Call<LoginModel>

    @POST("User/changepassword")
    @FormUrlEncoded
    fun getChangePasswordResponse(
        @Field("mobilenumber") mobileNumber: String,
        @Field("oldpassword") oldpassword: String,
        @Field("newpassword")
        newpassword: String
    ): Call<ChangePasswordModel>

    @POST("Userdetails/listallCustomers")
    @FormUrlEncoded
    fun getUserList(
        @Field("token") token: String,
        @Field("PageNumber") pageNumber: Int,
        @Field("RowsPerPage") rowsPerPage: Int
    ): Call<UserListModel>

    @POST("Userdetails/searchcustomermaster")
    @FormUrlEncoded
    fun getSearchResult(
        @Field("token") token: String,
        @Field("PageNumber") pageNumber: Int,
        @Field("RowsPerPage") rowsPerPage: Int,
        @Field("searchText") searchText: String?
    ): Call<UserListModel>

    @POST("Userdetails/getcustomerPackageList")
    @FormUrlEncoded
    fun getSubscribedChannelList(
        @Field("token") token: String,
        @Field("cust_id") pageNumber: Int,
        @Field("CpRowsPerPage") cpRowsPerPage: Int,
        @Field("CpPageNumber") cpPageNumber: Int?
    ): Call<SubscribedChannelsResponse>

    @POST("Userdetails/getAvailableChannelList")
    @FormUrlEncoded
    fun getAlacarteChannels(
        @Field("token") token: String,
        @Field("cust_id") customer_id: Int,
        @Field("ChannelRowsPerPage") channelRowsPerPage: Int,
        @Field("ChannelPageNumber") channelPageNumber: Int
    ): Call<AllChannelsResponse>

    @POST("Userdetails/GetAllPackageListByCustId")
    @FormUrlEncoded
    fun getAllChannels(
        @Field("token") token: String,
        @Field("cust_id") customer_id: Int,
        @Field("ChannelRowsPerPage") channelRowsPerPage: Int,
        @Field("ChannelPageNumber") channelPageNumber: Int
    ): Call<AllChannelsResponse>

    @POST("Userdetails/UpdatesubscribedChannelList")
    @FormUrlEncoded
    fun getUpdatesubscribedChannelList(
        @Field("token") token: String,
        @Field("cust_id") cust_id: Int,
        @Field("channel_ids") channel_ids: String
    ): Call<UpdatesubscribedChannelListResponse>


    @POST("Userdetails/SaveAllChannelsSelection")
    @FormUrlEncoded
    fun getSaveAllChannelsSelection(
        @Field("token") token: String,
        @Field("cust_id") cust_id: Int,
        @Field("channel_ids") channel_ids: String
    ): Call<UpdatesubscribedChannelListResponse>

    @POST("Userdetails/GetPendingBillBycustId")
    @FormUrlEncoded
    fun getGetPendingBillBycustId(
        @Field("token") token: String,
        @Field("cust_id") cust_id: Int
    ): Call<TotalAmountResponse>

    @POST("Userdetails/SavePaymentHistory")
    @FormUrlEncoded
    fun getSavePaymentHistory(
        @Field("token") token: String,
        @Field("cust_id") cust_id: Int,
        @Field("Billamount") billamount: Float,
        @Field("Paidamount") paidamount: Float
    ): Call<PaidAmountResponse>

    @POST("Userdetails/GetFullPaymentHistoryByCustId")
    @FormUrlEncoded
    fun getFullPaymentHistoryByCustId(
        @Field("token") token: String,
        @Field("cust_id") cust_id: Int
    ): Call<PaymentHistoryResponse>

    @POST("Userdetails/GetPrintBillDetails")
    @FormUrlEncoded
    fun getPrintBillDetails(
        @Field("token") token: String,
        @Field("cust_id") cust_id: Int
    ): Call<PrintBillResponse>

}