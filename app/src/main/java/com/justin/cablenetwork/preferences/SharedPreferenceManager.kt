package com.justin.cablenetwork.preferences

import android.content.Context
import android.content.SharedPreferences


 class SharedPreferenceManager(context: Context) {

     private var mSharedPreferences: SharedPreferences
     private var mEditor : SharedPreferences.Editor

    init {
        this.mSharedPreferences = context.getSharedPreferences("CableApplication", Context.MODE_PRIVATE)
        this.mEditor =  mSharedPreferences.edit()

    }

    companion object {

        internal var sharedPreferenceManager: SharedPreferenceManager? = null

        fun getInstance(context: Context): SharedPreferenceManager {
            if (sharedPreferenceManager == null) {
                sharedPreferenceManager =
                    SharedPreferenceManager(context)
            }
            return sharedPreferenceManager as SharedPreferenceManager
        }
    }

     fun putString(key : String, text : String){
         mEditor.putString(key,text)
         mEditor.commit()
     }

     fun getString(key : String) : String {
         return mSharedPreferences.getString(key,"")
     }

     fun putBoolean(key : String, boolean: Boolean ){
         mEditor.putBoolean(key,boolean)
         mEditor.commit()
     }

     fun getBoolean(key : String) : Boolean {
         return mSharedPreferences.getBoolean(key,false)
     }

     fun doLogout() {
         mEditor.clear()
         mEditor.commit()
     }

 }