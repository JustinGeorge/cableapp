package com.justin.cablenetwork.preferences

class PreferenceUtils {

    interface LoginConstants {
        companion object {
            val token = "token"
            val First_name = "First_name"
            val last_name = "last_name"
            val email_id = "email_id"
            val mobile = "mobile"
            val user_type = "user_type"
            val user_role = "user_role"
            val first_login_indicator = "unique_id"
            val user_login = "user_login"
        }
    }

    interface ChangePasswordConstants {
        companion object {
            val first_time_change_password = "first_time_change_password"
        }
    }

    interface PrintBillsConstants {
        companion object {
            val bluetoothprintername = "bluetoothprintername"
        }
    }
}